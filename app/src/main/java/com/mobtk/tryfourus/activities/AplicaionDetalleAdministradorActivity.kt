package com.mobtk.tryfourus.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.fragments.AdministradorUTesterFragment
import com.mobtk.tryfourus.fragments.AdministradorUsuariosFragment
import com.mobtk.tryfourus.fragments.PagosTesterAdministradorFragment
import com.mobtk.tryfourus.models.AppsCompanyModel

class AplicaionDetalleAdministradorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aplicaion_detalle_administrador)

        val img = findViewById<ImageView>(R.id.imgAplicacionDetalle)
        val nombre = findViewById<TextView>(R.id.tVNombreAplicacionDetalle)
        val version = findViewById<TextView>(R.id.tVVersionAplicacionDetalle)

        val datoAplicaion = intent.getSerializableExtra("DatosAplicaion" )as AppsCompanyModel
        Log.d("TAG", "Datos aplicaion "+datoAplicaion.toString())

        if(datoAplicaion.categoria == "1"){
            img.setImageResource(R.drawable.uber_logo)
        }
        if(datoAplicaion.categoria == "3"){
            img.setImageResource(R.drawable.frutas)
        }
        if(datoAplicaion.categoria == "2"){
            img.setImageResource(R.drawable.didi)
        }
        nombre.setText(datoAplicaion.name)
        version.setText("Versión: "+datoAplicaion.version)

        var actionBar = supportActionBar
        if(actionBar != null){
            actionBar.setTitle("Detalle aplicacion")
            actionBar.setDisplayShowHomeEnabled(true)
            actionBar.setDisplayShowCustomEnabled(true)
        }

        val administradorUTesterFragment = AdministradorUTesterFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.conten, administradorUTesterFragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }
}
