package com.mobtk.tryfourus.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.mobtk.tryfourus.R

class Splash : AppCompatActivity(), Runnable {

    private lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()
        simulaCarga()
    }

    private fun simulaCarga(){
        handler =  Handler()
        handler.postDelayed(this@Splash, 3000)
    }

    override fun run() {
        val intent = Intent(this@Splash, LoginActivity::class.java)
        startActivity(intent)
        finish()

    }
}
