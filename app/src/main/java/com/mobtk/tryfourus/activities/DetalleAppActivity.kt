package com.mobtk.tryfourus.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.mobtk.tryfourus.R
import kotlinx.android.synthetic.main.activity_detalle_app.*

class DetalleAppActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_app)

        val nombre = intent.getStringExtra("nombre")
        val nombreApp = findViewById<TextView>(R.id.textNombreApp)
        nombreApp.setText(nombre)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Detalle App"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
        btnGenerarR.setOnClickListener {
            val intent = Intent(this@DetalleAppActivity, GenerarReporteActivity::class.java)
            intent.putExtra("nombreApp", nombre)
            startActivity(intent)
        }

    }
        override fun onSupportNavigateUp(): Boolean {
            onBackPressed()
            return true
        }
}
