package com.mobtk.tryfourus.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.MenuItemImpl
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.fragments.*
import com.mobtk.tryfourus.utilities.SharedPreference
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var toolbar: Toolbar
    lateinit var toolbarText:TextView
    lateinit var drawerLayout: DrawerLayout
    lateinit var navViewAdministracion: NavigationView
    lateinit var administradorUsuariosFragment: AdministradorUsuariosFragment
    lateinit var administradorHomeFragment : AdministradorHomeFragment
    private var bandera:Int=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)
        val sharedPreference=SharedPreference(this)
        bandera = sharedPreference.getValueInt("usuario")
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navViewAdministracion = findViewById(R.id.nav_administrador)
        navViewAdministracion.setNavigationItemSelectedListener(this)
        if(null == savedInstanceState) {
            if (bandera==2){
                val blankFragment = HomeCompanyFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, blankFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }

            else if (bandera == 3){
                val testerHomeFragment = HomeTesterFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, testerHomeFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
        }
        setBottomMenu()
        onToolbarName()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        //Modulo  de administrador
        if (bandera==1){
        when (item.itemId) {
            R.id.nav_inicio -> {
                lateinit var administradorHomeFragment : AdministradorHomeFragment
                administradorHomeFragment = AdministradorHomeFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, administradorHomeFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                bottom_navigation_admin.selectedItemId=R.id.action_home
                onToolbarName()
            }
            R.id.nav_perfil -> {
                val profileFragment = ProfileFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, profileFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                bottom_navigation_admin.selectedItemId=R.id.action_profile
                onToolbarName()
            }
            R.id.nav_pagos -> {
                val pagosAdministradorFragment = PagosAdministradorFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, pagosAdministradorFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                bottom_navigation_admin.selectedItemId=R.id.action_pays
                onToolbarName()
            }
            R.id.nav_administracion -> {
                administradorUsuariosFragment = AdministradorUsuariosFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, administradorUsuariosFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                bottom_navigation_admin.selectedItemId=R.id.action_upload
                onToolbarName()
            }
            R.id.nav_bugs -> {
                var fragment = BugsFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                bottom_navigation_admin.selectedItemId=R.id.action_bugs
                onToolbarName()
                var sharedPreference = SharedPreference(this)
                sharedPreference.save("bugs",0)
            }
            R.id.nav_Notificacion -> {
                val notificacionesFragment= NotificacionesFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, notificacionesFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                bottom_navigation_admin.deselectAllItems()
                onToolbarName()
            }
            R.id.nav_ayuda->{
                val ayudaFragment = PreguntasFrecuentesFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, ayudaFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                bottom_navigation_admin.deselectAllItems()
                onToolbarName()
            }
            R.id.nav_sesion -> {
                val alerDialog = AlertDialog.Builder(this)
                alerDialog.setTitle("Alerta")
                alerDialog.setMessage("Cerrar sesión?")
                alerDialog.setPositiveButton("Si"){dialog, which ->
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                }
                alerDialog.setNegativeButton("No"){dialog, which ->

                }
                val dialog = alerDialog.create()
                dialog.show()
            }

        }
    }
        else if(bandera==2){
            when (item.itemId) {
                R.id.nav_inicio_empresa -> {
                    val homeFragment = HomeCompanyFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, homeFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    supportFragmentManager.executePendingTransactions()
                    bottom_navigation.selectedItemId=R.id.action_home
                    onToolbarName()

                    var sharedPreference = SharedPreference(this)
                    sharedPreference.save("bugs",0)
                }
                R.id.nav_perfil_empresa -> {
                    val profileFragment =ProfileFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, profileFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    supportFragmentManager.executePendingTransactions()

                    bottom_navigation.selectedItemId=R.id.action_profile
                    onToolbarName()
                }
                R.id.nav_subir_app_empresa -> {
                    val subirAppsFragment = SubirAppsFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout,subirAppsFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()

                    supportFragmentManager.executePendingTransactions()

                    bottom_navigation.selectedItemId=R.id.action_upload
                    onToolbarName()
                }
                R.id.nav_pagos_empresa -> {
                    val pagosFragment = EmpresaPagosFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout,pagosFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()

                    supportFragmentManager.executePendingTransactions()

                    bottom_navigation.selectedItemId=R.id.action_pays
                    onToolbarName()
                }
                R.id.nav_bugs_empresa -> {
                    val bugsFragment = BugsFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, bugsFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    supportFragmentManager.executePendingTransactions()

                    bottom_navigation.selectedItemId=R.id.action_bugs
                    onToolbarName()
                    var sharedPreference = SharedPreference(this)
                    sharedPreference.save("bugs",1)
                }
                R.id.nav_notificacion_empresa -> {
                    val notificacionesFragment = NotificacionesFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, notificacionesFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()

                    bottom_navigation.deselectAllItems()
                    supportFragmentManager.executePendingTransactions()
                    onToolbarName()
                }
                R.id.nav_ayuda-> {
                    val ayudaFragment = PreguntasFrecuentesFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, ayudaFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    supportFragmentManager.executePendingTransactions()
                    bottom_navigation.deselectAllItems()
                    onToolbarName()
                }
                R.id.nav_sesion -> {
                    onBackPressed()
                }
            }
        }
        else if(bandera==3){

            when(item.itemId){
                R.id.nav_inicio_tester -> {
                    val homeFragmentester = HomeTesterFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, homeFragmentester)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    supportFragmentManager.executePendingTransactions()
                    bottom_navigation_tester.selectedItemId=R.id.action_homeTester
                    onToolbarName()
                }

                R.id.nav_perfil_tester -> {
                    val profileFragmenttester = ProfileFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, profileFragmenttester)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    supportFragmentManager.executePendingTransactions()
                    bottom_navigation_tester.selectedItemId=R.id.action_profileTester
                    onToolbarName()

                }
                R.id.nav_pagos_tester -> {
                    val pagosTeterFragment = EmpresaPagosFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, pagosTeterFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    supportFragmentManager.executePendingTransactions()
                    bottom_navigation_tester.selectedItemId=R.id.action_paysTester
                    onToolbarName()
                }

                R.id.nav_bugs_tester -> {
                    val bugsTester = BugsFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, bugsTester)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    supportFragmentManager.executePendingTransactions()
                    bottom_navigation_tester.selectedItemId=R.id.action_bugsTester
                    onToolbarName()
                    var sharedPreference = SharedPreference(this)
                    sharedPreference.save("bugs",1)
                }
                R.id.nav_Notificacion_tester -> {

                    var notifiTesterFragment: NotificacionesFragment
                    notifiTesterFragment = NotificacionesFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, notifiTesterFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    supportFragmentManager.executePendingTransactions()
                    bottom_navigation_tester.selectedItemId=R.id.action_notificationTester
                    onToolbarName()
                }
                R.id.nav_ayuda_tester-> {
                    val ayudaFragment = PreguntasFrecuentesFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, ayudaFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    supportFragmentManager.executePendingTransactions()
                    bottom_navigation_tester.deselectAllItems()
                    onToolbarName()
                }
                R.id.nav_sesion -> {
                    onBackPressed()
                }

            }

        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    fun setBottomMenu(){
        if(bandera==1) {
            navViewAdministracion.menu.clear()
            navViewAdministracion.inflateMenu(R.menu.nav_menu_administrador)
            administradorHomeFragment = AdministradorHomeFragment()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, administradorHomeFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()
            bottom_navigation.visibility = View.GONE
            bottom_navigation_tester.visibility = View.GONE
            bottom_navigation_admin.setOnNavigationItemSelectedListener {item ->
                when(item.itemId){
                    R.id.action_home -> {
                        val homeFragment = AdministradorHomeFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_layout, homeFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }
                    R.id.action_profile -> {
                        val profileFragment =ProfileFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_layout, profileFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }
                    R.id.action_pays -> {
                        val pagosFragment = PagosAdministradorFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_layout,pagosFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()

                    }
                    R.id.action_upload -> {
                        val adminUsers = AdministradorUsuariosFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_layout,adminUsers)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()

                    }
                    R.id.action_bugs ->{
                        val bugsFragment = BugsFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_layout, bugsFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }
                }
                supportFragmentManager.executePendingTransactions()
                onToolbarName()
                return@setOnNavigationItemSelectedListener true
            }
        }
        else if (bandera==2){
            navViewAdministracion.menu.clear()
            navViewAdministracion.inflateMenu(R.menu.nav_menu_empresa)
            val header=navViewAdministracion.getHeaderView(0)
            var image=header.findViewById<ImageView>(R.id.image_profile)
            var tipoUsuario=header.findViewById<TextView>(R.id.kind_user)
            var nombreUsuario=header.findViewById<TextView>(R.id.name_user)
            tipoUsuario.text="Empresa"
            nombreUsuario.text="Soultech"
            image.setImageDrawable(resources.getDrawable(R.drawable.ic_buildings_couple,baseContext.theme))
            var sharedPreference = SharedPreference(this)
            sharedPreference.save("bugs",0)
            bottom_navigation_tester.visibility = View.GONE
            bottom_navigation_admin.visibility = View.GONE
            bottom_navigation.setOnNavigationItemSelectedListener {item ->
                when(item.itemId){
                    R.id.action_home -> {
                        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                            if (it is HomeCompanyFragment){
                            }else{
                                val homeFragment = HomeCompanyFragment()
                                supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.frame_layout, homeFragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commit()
                            }
                        }
                    }
                    R.id.action_profile -> {
                        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                            if (it is ProfileFragment){
                            }else{
                                val profileFragment =ProfileFragment()
                                supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.frame_layout, profileFragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commit()
                            }
                        }
                    }
                    R.id.action_upload -> {
                        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                            if (it is SubirAppsFragment){
                            }else{
                                val subirAppsFragment = SubirAppsFragment()
                                supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.frame_layout,subirAppsFragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commit()
                            }
                        }
                    }
                    R.id.action_pays -> {
                        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                            if (it is EmpresaPagosFragment){
                            }else{
                                val pagosFragment = EmpresaPagosFragment()
                                supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.frame_layout,pagosFragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commit()
                            }
                        }
                    }
                    R.id.action_bugs ->{
                        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                            if (it is BugsFragment){

                            }else{
                                val bugsFragment = BugsFragment()
                                supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.frame_layout, bugsFragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commit()
                            }
                        }

                    }
                }
                supportFragmentManager.executePendingTransactions()
                onToolbarName()
                return@setOnNavigationItemSelectedListener true
            }
        }
        else if(bandera == 3){

            navViewAdministracion.menu.clear()
            navViewAdministracion.inflateMenu(R.menu.nav_menu_tester)
            val header= navViewAdministracion.getHeaderView(0)
            var image=header.findViewById<ImageView>(R.id.image_profile)
            var usuariotester=header.findViewById<TextView>(R.id.kind_user)
            var nameTester=header.findViewById<TextView>(R.id.name_user)
            usuariotester.text = "Tester"
            nameTester.text = "Laura Cortes"
            image.setImageDrawable(resources.getDrawable(R.drawable.testerperfil,baseContext.theme))
            var sharedPreference = SharedPreference(this)
            sharedPreference.save("bugs",0)
            bottom_navigation.visibility = View.GONE
            bottom_navigation_admin.visibility = View.GONE
            bottom_navigation_tester.setOnNavigationItemSelectedListener {item ->
                when(item.itemId){
                    R.id.action_homeTester -> {
                        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                            if (it is HomeTesterFragment){
                            }else{
                                val homeFragmenttester = HomeTesterFragment()
                                supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.frame_layout, homeFragmenttester)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commit()
                            }
                        }

                    }
                    R.id.action_profileTester -> {
                        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                            if (it is ProfileFragment){
                            }else{
                                val profileFragment =ProfileFragment()
                                supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.frame_layout, profileFragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commit()
                            }
                        }

                    }

                    R.id.action_paysTester -> {
                        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                            if (it is EmpresaPagosFragment){
                            }else{
                                val pagosFragment = EmpresaPagosFragment()
                                supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.frame_layout,pagosFragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commit()
                            }
                        }

                    }
                    R.id.action_bugsTester ->{
                        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                            if (it is BugsFragment){

                            }else{
                                val bugsFragment = BugsFragment()
                                supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.frame_layout, bugsFragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commit()
                            }
                        }

                    }
                    R.id.action_notificationTester -> {
                        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                            if (it is NotificacionesFragment){

                            }else{
                                val notificacionesFragment = NotificacionesFragment()
                                supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.frame_layout,notificacionesFragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commit()
                            }
                        }
                    }
                }
                supportFragmentManager.executePendingTransactions()
                onToolbarName()
                return@setOnNavigationItemSelectedListener true
            }

        }
    }
    private fun onToolbarName(){
        toolbarText = toolbar.findViewById(R.id.tVnameToolbar)
            supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                // the fragment exists
                if (bandera == 1){

                    if (it is PagosAdministradorFragment) {
                        toolbarText.text = "Pagos"
                    }
                    if (it is AdministradorUsuariosFragment) {
                        toolbarText.text="Administración"
                    }
                    if (it is AdministradorHomeFragment){
                        toolbarText.text="Inicio"
                    }
                    if (it is ProfileFragment) {
                        toolbarText.text="Perfil"
                    }


                }else if (bandera==2) {
                    if (it is EmpresaPagosFragment) {
                        toolbarText.text = "Pagos"
                    }
                    if (it is SubirAppsFragment) {
                        toolbarText.text="Subir app"
                    }
                    if (it is HomeCompanyFragment){
                        toolbarText.text="Inicio"
                    }
                    if (it is ProfileFragment) {
                        toolbarText.text="Perfil"
                    }
                }

                else if (bandera == 3){

                    if (it is EmpresaPagosFragment) {
                        toolbarText.text = "Pagos"
                    }
                    if (it is HomeTesterFragment){
                        toolbarText.text="Inicio"
                    }
                    if (it is ProfileFragment) {
                        toolbarText.text="Perfil"
                    }

                }


                if (it is BugsFragment) {
                    toolbarText.text="Bugs"
                }
                if (it is PreguntasFrecuentesFragment) {
                    toolbarText.text="Ayuda"
                }
                if (it is NotificacionesFragment) {
                    toolbarText.text="Notificaciones"
                }
            }



        }


    override fun onBackPressed() {
        //super.onBackPressed()
        val alerDialog = AlertDialog.Builder(this)
        alerDialog.setTitle("Alerta")
        alerDialog.setMessage("Cerrar sesión?")
        alerDialog.setPositiveButton("Si"){dialog, which ->
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        alerDialog.setNegativeButton("No"){dialog, which ->
        }
        val dialog = alerDialog.create()
        dialog.show()

    }

    private fun BottomNavigationView.deselectAllItems() {
        val menu = this.menu
        for(i in 0 until menu.size()) {
            (menu.getItem(i) as? MenuItemImpl)?.let {
                it.isExclusiveCheckable = false
                it.isChecked = false
                it.isExclusiveCheckable = true
            }
        }
    }
}
