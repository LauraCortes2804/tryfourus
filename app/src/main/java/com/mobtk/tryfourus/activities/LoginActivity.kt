package com.mobtk.tryfourus.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.databinding.ActivityLoginBinding
import com.mobtk.tryfourus.utilities.SharedPreference

class LoginActivity : AppCompatActivity(), Validator.ValidationListener {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityLoginBinding>(this,
            R.layout.activity_login)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val validator = Validator(binding)
        supportActionBar?.hide()
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.LbtnIniciar.id -> {
                    validator.toValidate()
                }
                binding.btnViewPass.id ->{
                    binding.edtPassword.setSelection(binding.edtPassword.length())
                    if (binding.edtPassword.transformationMethod is PasswordTransformationMethod) {
                        binding.edtPassword.transformationMethod = null
                    } else {
                        binding.edtPassword.transformationMethod = PasswordTransformationMethod()
                    }
                    binding.edtPassword.setSelection(binding.edtPassword.length())
                }
                binding.tvLRegistrar.id ->{
                    val intent = Intent(this, PasswordActivity::class.java)
                    startActivity(intent)
                }
                binding.btnOlvideN.id -> {
                    val intent = Intent(this, RegistroActivity::class.java)
                    startActivity(intent)
                }
            }
        }
    }

    override fun onValidationSuccess() {
        var bandera = binding.edtPassword.text.toString().toInt()
        val sharedPreferences= SharedPreference(this)
        sharedPreferences.save("usuario", bandera)


        if(bandera == 3){
            val intent = Intent(this, OnboardingTester::class.java)
            startActivity(intent)
            finish()

        } else{

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()

        }


    }

    override fun onValidationError() {
        Toast.makeText(this, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }
}
