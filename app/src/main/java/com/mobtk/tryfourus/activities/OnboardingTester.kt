package com.mobtk.tryfourus.activities

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.OnboardingAdapter
import com.mobtk.tryfourus.fragments.HomeTesterFragment

class OnboardingTester : AppCompatActivity() , View.OnClickListener {

    lateinit var mPager: ViewPager
    var layouts: IntArray = intArrayOf(R.layout.primer_slider_tester, R.layout.segundo_slider_tester, R.layout.tercer_slider_tester)
    lateinit var dotsLayout: LinearLayout
    lateinit var dots: Array<ImageView>
    lateinit var mAdapter: OnboardingAdapter
    lateinit var btnNext: Button
    lateinit var btnSkip: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        supportActionBar?.hide()



        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        mPager = findViewById(R.id.pager)
        mAdapter = OnboardingAdapter(layouts, this)
        mPager.adapter = mAdapter
        dotsLayout = findViewById(R.id.dots)
        btnSkip = findViewById(R.id.btnSkip)
        btnNext = findViewById(R.id.btnNext)
        btnSkip.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        createDots(0)
        mPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,


                positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                createDots(position)

                if (position == layouts.size - 1) {
                    btnNext.setText(R.string.iniciar)
                    btnSkip.visibility = View.INVISIBLE
                } else {
                    btnNext.setText(R.string.siguiente)
                    btnSkip.visibility = View.VISIBLE
                }
            }
        })
    }

    override fun onClick(v: View?) {

        when(v!!.id) {
            R.id.btnSkip -> {
                loadHome()

            }

            R.id.btnNext -> {
                loadNextSlide()

            }
        }
    }

    private fun loadNextSlide() {
        val nextSlide: Int = mPager.currentItem + 1

        if (nextSlide < layouts.size) {
            mPager.setCurrentItem(nextSlide)
        } else {
            loadHome()
        }
    }

    private fun loadHome() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    fun createDots(position: Int){
        if(dotsLayout!= null) {
            dotsLayout.removeAllViews()
        }
        dots = Array(layouts.size) { i -> ImageView(this)}

        for (i in layouts.indices) {

            dots[i] = ImageView(this)
            if (i == position) {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots))
            }
            else{
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.inactive_dots))
            }

            val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)

            params.setMargins(4, 0, 4,0)
            dotsLayout.addView(dots[i], params)
        }
    }
}