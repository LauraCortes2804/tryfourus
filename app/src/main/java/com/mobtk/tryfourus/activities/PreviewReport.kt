package com.mobtk.tryfourus.activities

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.myapplication.FragmentAdmin.PageUnoFragment
import com.example.myapplication.FragmentTeste.PageDosFragment
import com.example.myapplication.PageTresFragment
import com.github.paolorotolo.appintro.AppIntro2
import com.mobtk.tryfourus.fragments.HomeTesterFragment
import kotlinx.android.synthetic.main.fragment_page_tres.*

class PreviewReport : AppIntro2() {

    override fun onCreate(savedInstanceState: Bundle?) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState)

        //setContentView(R.layout.activity_main);
        addSlide(PageUnoFragment())  //extend AppIntro and comment setContentView
        addSlide(PageDosFragment())
        addSlide(PageTresFragment())
        setBarColor(Color.parseColor("#12a00d"))
        //setSeparatorColor(Color.parseColor("#2196F3"))
        showSkipButton(false)
        //setProgressButtonEnabled(true)
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        //super.onDonePressed(currentFragment)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        //this.finish()

        var preguntaU = edtPreguntaUno.text.toString().toLowerCase()
        Log.e("Tag", "--->"+preguntaU)
        var preguntaD = edtPreguntaDos.text.toString().toLowerCase()
        if (preguntaU == "si" || preguntaD == "no"){
            var intent = Intent(this, CertificadoActivity::class.java)
            startActivity(intent)
            finish()
        }else{
            val dialogBuilder = AlertDialog.Builder(this)

            // set message of alert dialog
            dialogBuilder.setMessage("Examen no aprobado Puedes intentarlo de nuevo, ÉXITO ")
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("Ok", DialogInterface.OnClickListener {
                        dialog, id -> finish()
                    this.finish()
                })
                // negative button text and action

            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle("Evaluacion")
            // show alert dialog
            alert.show()
        }
    }

    override fun onSlideChanged(oldFragment: Fragment?, newFragment: Fragment?) {
        super.onSlideChanged(oldFragment, newFragment)
        // Do something when the slide changes.
    }
}