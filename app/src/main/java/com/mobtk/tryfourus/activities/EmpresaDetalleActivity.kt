package com.mobtk.tryfourus.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.mobtk.tryfourus.utilities.Constantes
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.CompanyAppsAdapter.Companion.NOMBRE
import com.mobtk.tryfourus.adapter.EmpresaDetalleAdapter
import com.mobtk.tryfourus.models.UsuariosTesterModel
import com.mobtk.tryfourus.task.AppTask
import com.mobtk.tryfourus.task.TesterTask
import kotlinx.android.synthetic.main.activity_empresa_detalle.*

import kotlinx.android.synthetic.main.carga.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class EmpresaDetalleActivity : AppCompatActivity() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var testers: ArrayList<UsuariosTesterModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empresa_detalle)
        nombreApp.text = intent.getStringExtra(Constantes.NOMBRE)
        testers = ArrayList()
        testers()
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Detalle Empresa"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        val requestManager = Glide.with(this)
        val requestBuilder = requestManager.load("https://d3i4yxtzktqr9n.cloudfront.net/uber-sites/f452c7aefd72a6f52b36705c8015464e.jpg")
        requestBuilder.into(imagEmpreDetalle)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    private fun testers(){
        doAsync {
            val tester = TesterTask.getTesters(getString(R.string.base_url))
            uiThread {
                linearLayoutManager = LinearLayoutManager(this@EmpresaDetalleActivity, LinearLayoutManager.VERTICAL, false)
                rvEmpresaDetalle.layoutManager = linearLayoutManager
                testers = tester.usuariosTester
                rvEmpresaDetalle.adapter = EmpresaDetalleAdapter(testers)

                avi.hide()
                carga.visibility= View.GONE
            }
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}