package com.mobtk.tryfourus.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.CategoriasTesterAdapter
import com.mobtk.tryfourus.databinding.ActivityAppCategoriasTesterBinding
import com.mobtk.tryfourus.models.AppsCompanyModel
import com.mobtk.tryfourus.task.AppTask
import kotlinx.android.synthetic.main.carga.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class AppCategoriasTesterActivity : AppCompatActivity() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var apps: List<AppsCompanyModel>

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityAppCategoriasTesterBinding>(this, R.layout.activity_app_categorias_tester)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_categorias_tester)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Categorias"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        apps = ArrayList()
        aplicacionesTester()
        aplicacionesSinTestear()
        aplicacionesTesteadas()
    }


    @SuppressLint("WrongConstant")
    private fun aplicacionesTester(){

        doAsync {
            val response = AppTask.getApp(getString(R.string.base_url))

            uiThread {
                linearLayoutManager = LinearLayoutManager(applicationContext, OrientationHelper.HORIZONTAL, false)
                binding.rvApps.layoutManager = linearLayoutManager
                apps = response.aplica
                binding.rvApps.adapter = CategoriasTesterAdapter(apps)
                var layaout2 = findViewById<RelativeLayout>(R.id.primerTesterHomeRV)
                layaout2?.aviPac?.hide()
                layaout2?.cargaRv?.visibility = View.GONE
            }
        }
    }


    @SuppressLint("WrongConstant")
    private fun aplicacionesSinTestear(){

        doAsync {
            val response = AppTask.getApp(getString(R.string.base_url))

            uiThread {
                linearLayoutManager = LinearLayoutManager(applicationContext, OrientationHelper.HORIZONTAL, false)
                binding.rvAppSinTestear.layoutManager = linearLayoutManager
                apps = response.aplica
                binding.rvAppSinTestear.adapter = CategoriasTesterAdapter(apps)
                var layaout2 = findViewById<RelativeLayout>(R.id.segundoTesterHomeRV)
                layaout2?.aviPac?.hide()
                layaout2?.cargaRv?.visibility = View.GONE
            }
        }
    }

    @SuppressLint("WrongConstant")
    private fun aplicacionesTesteadas(){

        doAsync {
            val response = AppTask.getApp(getString(R.string.base_url))

            uiThread {
                linearLayoutManager = LinearLayoutManager(applicationContext, OrientationHelper.HORIZONTAL, false)
                binding.rvAppTesteadas.layoutManager = linearLayoutManager
                apps = response.aplica
                binding.rvAppTesteadas.adapter = CategoriasTesterAdapter(apps)
                var layaout2 = findViewById<RelativeLayout>(R.id.terceroTesterHomeRV)
                layaout2?.aviPac?.hide()
                layaout2?.cargaRv?.visibility = View.GONE
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
