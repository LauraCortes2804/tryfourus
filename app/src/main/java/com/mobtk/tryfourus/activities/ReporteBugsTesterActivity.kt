package com.mobtk.tryfourus.activities

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.utilities.SharedPreference
import kotlinx.android.synthetic.main.item_bugs.*


class ReporteBugsTesterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_bugs)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Reporte"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        val tipoUsuario= SharedPreference(this).getValueInt("usuario")
        // Asignar vistas
        var nombre2 = findViewById<TextView>(R.id.tvNombreApp)
        var bugCategoria2 = findViewById<TextView>(R.id.tvNivelBug)
        var bugDescripcion2 = findViewById<TextView>(R.id.tvDescripcion)

        nombre2.text=getString(R.string.dummy_name_app)
        bugCategoria2.text="3"
        bugDescripcion2.text=getString(R.string.dummy_text)

        if (tipoUsuario==1){
            val nombre = intent.getStringExtra("bug_nombre")
            val bugDescripcion = intent.getStringExtra("bug_desc")
            val bugCategoria = intent.getStringExtra("bug_cat")

            nombre2.setText(nombre)
            bugCategoria2.setText(bugCategoria)
            bugDescripcion2.setText(bugDescripcion)
            approve_zone.visibility= View.GONE
        }

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
