package com.mobtk.tryfourus.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.fragments.DetalleAdministradorTestesrInfoFragment
import com.mobtk.tryfourus.fragments.DetalleAdministradorUTestesrAppsFragment
import com.mobtk.tryfourus.utilities.ViewPageAdapter
import kotlinx.android.synthetic.main.activity_detalle_administrador_utester.*

class DetalleAdministradorUTesterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_administrador_utester)

        var actionBar = supportActionBar

        if (actionBar != null){
            actionBar.setTitle("Administración de usuarios")
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDefaultDisplayHomeAsUpEnabled(true)
        }

        var nombre : String? =  intent.getStringExtra("nombre_tester")
        var apellidos : String? = intent.getStringExtra("apellidos_tester")

        nombre_usu_post.text = "$nombre $apellidos"

        val adapterA = ViewPageAdapter (supportFragmentManager)
        adapterA.addFragment(DetalleAdministradorTestesrInfoFragment(), "Info")
        adapterA.addFragment(DetalleAdministradorUTestesrAppsFragment(), "Apps Report")
        viewPagerU.adapter = adapterA
        tabsU.setupWithViewPager(viewPagerU)

    }
}


