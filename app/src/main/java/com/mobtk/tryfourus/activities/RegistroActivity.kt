package com.mobtk.tryfourus.activities

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.databinding.ActivityRegistroBinding
import kotlinx.android.synthetic.main.activity_registro.*
import org.jetbrains.anko.alert

class RegistroActivity : AppCompatActivity(), Validator.ValidationListener  {
    lateinit var lyuser: LinearLayout
    lateinit var lyempresa: LinearLayout

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityRegistroBinding>(this, R.layout.activity_registro)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)

        lyuser = binding.lyUsuario
        lyempresa = binding.lyEmpresa
        binding.setClickListener {
            when (it!!.id) {
                binding.btnRUsuario.id -> {
                    btnRUsuario.setBackgroundColor(Color.parseColor("#5ad247"))
                    btnREmpresa.setBackgroundColor(Color.parseColor("#ffffff"))
                    tvUsuario.setTextColor(Color.parseColor("#ffffff"))
                    tvEmpresa.setTextColor(Color.parseColor("#5ad247"))
                    lyuser.visibility = View.VISIBLE
                    lyempresa.visibility = View.GONE
                }
                binding.btnREmpresa.id -> {
                    btnRUsuario.setBackgroundColor(Color.parseColor("#ffffff"))
                    btnREmpresa.setBackgroundColor(Color.parseColor("#5ad247"))
                    tvUsuario.setTextColor(Color.parseColor("#5ad247"))
                    tvEmpresa.setTextColor(Color.parseColor("#ffffff"))
                    lyuser.visibility = View.GONE
                    lyempresa.visibility = View.VISIBLE
                }
                binding.btnRegistarE.id ->{
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                binding.btnregistrarU.id ->{
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }

    override fun onValidationSuccess() {
        Toast.makeText(this,"Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this@RegistroActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }

}