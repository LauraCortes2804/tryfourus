package com.mobtk.tryfourus.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import com.mobtk.tryfourus.R
import kotlinx.android.synthetic.main.activity_generar_reporte.*

class GenerarReporteActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generar_reporte)

        val nombre = intent.getStringExtra("nombreApp")
        val nombreApp = findViewById<TextView>(R.id.nombreGenerar)
        nombreApp.setText(nombre)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Generar Reporte"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
        btnFloatCheck.setOnClickListener {
            Toast.makeText(applicationContext,"Reporte Generado", Toast.LENGTH_LONG).show()
            onBackPressed()
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
