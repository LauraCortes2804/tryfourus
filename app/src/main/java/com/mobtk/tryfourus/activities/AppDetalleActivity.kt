package com.mobtk.tryfourus.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.mobtk.tryfourus.R

class AppDetalleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_detalle)

        val name = intent.getStringExtra("nombre")
        val nameTv = findViewById<TextView>(R.id.tvAppDetailApp)

        nameTv.setText(name)
    }
}
