package com.mobtk.tryfourus.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.fragments.HomeTesterFragment
import kotlinx.android.synthetic.main.activity_certificado.*

class CertificadoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_certificado)

        btnVerificarE.setOnClickListener {
            this.finish()
        }
    }
}
