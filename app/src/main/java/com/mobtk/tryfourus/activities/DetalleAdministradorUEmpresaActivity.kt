package com.mobtk.tryfourus.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.fragments.DetalleAdministradorUEmpresaAppsFragment
import com.mobtk.tryfourus.fragments.DetalleAdministradorUEmpresaInfoFragment
import com.mobtk.tryfourus.utilities.ViewPageAdapter
import kotlinx.android.synthetic.main.activity_detalle_administrador_uempresa.*

class DetalleAdministradorUEmpresaActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_administrador_uempresa)

        var actionBar = supportActionBar

        if (actionBar != null){
            actionBar.setTitle("Administración de usuarios")
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDefaultDisplayHomeAsUpEnabled(true)
        }

        nombre_emp_post.text = intent.getStringExtra("nombre_empresa")

        val adapterA = ViewPageAdapter (supportFragmentManager)
        adapterA.addFragment(DetalleAdministradorUEmpresaInfoFragment(), "Info")
        adapterA.addFragment(DetalleAdministradorUEmpresaAppsFragment(), "Apps Report")
        viewPagerE.adapter = adapterA
        tabsE.setupWithViewPager(viewPagerE)
    }
}


