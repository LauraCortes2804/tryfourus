package com.mobtk.tryfourus.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.mobtk.tryfourus.R
import kotlinx.android.synthetic.main.activity_password.*

class PasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password)

        val actionBar = supportActionBar
        if(actionBar !=  null){
            actionBar.setTitle("Recuperar Paswword")
            actionBar.setDisplayShowCustomEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }


        val textView_contraeña = findViewById<EditText>(R.id.edit_contraseña_recuperar)


        btn_recuperar_contraseña.setOnClickListener{
            if(textView_contraeña.getText().toString().trim().equals("")){
                Toast.makeText(this, "Es necesario ingresar el su correo", Toast.LENGTH_SHORT).show()
            }else{
                val builderAler = AlertDialog.Builder(this@PasswordActivity)
                builderAler.setTitle("Contraseña enviada")
                builderAler.setMessage("Te hemos enviado una contraseña a tu correo electronico")
                builderAler.setPositiveButton("Ok" ){_,_->
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }

                var dialog: AlertDialog =builderAler.create()
                dialog.show()

            }
        }
    }
}
