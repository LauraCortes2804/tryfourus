package com.mobtk.tryfourus.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mobtk.tryfourus.R
import kotlinx.android.synthetic.main.activity_detalle_app_administrador.*

class DetalleAppAdministradorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_app_administrador)

        var actionBar = supportActionBar

        if (actionBar != null){
            actionBar.setTitle("Administración de usuarios")
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDefaultDisplayHomeAsUpEnabled(true)
        }

        val nameApps = intent.getStringExtra("nombre_app")
        PostTextAppsA.text = nameApps

    }
}
