package com.mobtk.tryfourus.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mobtk.tryfourus.R

class AgregarCursoAdministradorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar_curso_administrador)

        var actionBar = supportActionBar

        if (actionBar != null){
            actionBar.setTitle("Detalles")
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDefaultDisplayHomeAsUpEnabled(true)
        }

    }
}
