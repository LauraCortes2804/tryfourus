package com.mobtk.tryfourus.interfaz

import com.mobtk.tryfourus.responses.AppsCompanyResponse
import com.mobtk.tryfourus.responses.NotificacionesResponse
import com.mobtk.tryfourus.responses.PagosResponse
import com.mobtk.tryfourus.responses.*
import retrofit2.Call
import retrofit2.http.GET

interface ApiService{

    @GET("MiguelRamirezAgustin/9e7ee36a0a0977addb1b74657c925208/raw/cb99178bef4a0316aeaf9a4985781dce1ed10cac/usuariosTester")
    fun getTester(): Call<UsuariosTestersResponse>

    @GET("MiguelRamirezAgustin/015f05bf26ad072ed707d6122e45981d/raw/53ec5e45cf92414645b35d05ee269b22dd7cdccb/usuariosEmpresas")
    fun getEmpresa(): Call<UsuariosEmpresasResponse>

    @GET("MiguelRamirezAgustin/673967a0e5b1393f4b309c3237b30ed8/raw/03840f83e2149325b2d69c47648925be0fe32a5a/cobrosEmpresa")
    fun getCobrosEmpresaAdmin():Call<CobrosEmpresaAdminResponse>

    @GET("MiguelRamirezAgustin/e84d4896e1e0240d937914136a285c6d/raw/f517b70e69ab6f393813f2a7036cb28cf06b6916/pagosGeneral")
    fun getPagos():Call<PagosResponse>

    @GET("JosemLop/cda5152c3565ccc8980b11cba5476477/raw/7d9ec4a2f9a7eec27525f1f9a715e66d67581779/ReportesBugs")
    fun bugsTester():Call<BugsResponse>

    @GET("MiguelRamirezAgustin/5ac52ac53cb92b985ffa09dd66e27646/raw/fa979b0e4cba4f4f356ca00fc5eecc1239efec15/aplicaciones")
    fun getApps(): Call<AppsCompanyResponse>

    @GET("MiguelRamirezAgustin/625154f039a27f0ab541d733bc8cf940/raw/bda9dc6b0c87edb8848ffaa9153b9621441105c1/notificaciones")
    fun getNotificaciones(): Call<NotificacionesResponse>

    @GET("MiguelRamirezAgustin/bf04bfc74a8fa8263329fa36c08fd0d0/raw/00e45d3a28d147fe5e998b6ea878275af392c5e8/cursos")
    fun cursos(): Call<CursosResponse>

    @GET("MiguelRamirezAgustin/024983f989bbda2fe8433a49654878cf/raw/4a5a81e0627b634d2dac70e4ca6c923125d3037b/preguntas")
    fun getPreguntas(): Call<PreguntasResponse>

    @GET("LauraCortes04/be0b61f3edc22a517f3a764c38e67552/raw/da53da5cf3f2b26e2a30f68cd2a76b3e5fbb4bc6/CategoriasApp")
    fun categoriasApps (): Call<CategoriasAppsResponse>

}