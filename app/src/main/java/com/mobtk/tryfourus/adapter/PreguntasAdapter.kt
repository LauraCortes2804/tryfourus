package com.mobtk.tryfourus.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.databinding.ItemPreguntasFrecuentesBinding
import com.mobtk.tryfourus.models.PreguntaModel


class PreguntasAdapter(private val items: ArrayList<PreguntaModel>) :
    RecyclerView.Adapter<PreguntasAdapter.ViewHolder>() {
    private var contexto: Context? = null
    lateinit var binding: ItemPreguntasFrecuentesBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(contexto)
        binding = ItemPreguntasFrecuentesBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding(items[position])
        binding.setClickListener {
            when(it!!.id){
                holder.dataBindig.btnShow.id->{
                    if (holder.dataBindig.respuesta.visibility== View.GONE){
                        holder.dataBindig.respuesta.visibility= View.VISIBLE
                        holder.dataBindig.btnShow.setBackgroundResource(R.drawable.ic_remove_circle_outline_24px)}
                    else{
                        holder.dataBindig.respuesta.visibility= View.GONE
                        holder.dataBindig.btnShow.setBackgroundResource(R.drawable.ic_add_circle_outline_24px)
                    }
                }
            }
        }
    }


    class ViewHolder(val dataBindig: ItemPreguntasFrecuentesBinding) : RecyclerView.ViewHolder(dataBindig.root) {
        fun binding(modelPreguntas: PreguntaModel) {
            dataBindig.elemento = modelPreguntas
            dataBindig.executePendingBindings()
        }
    }
}