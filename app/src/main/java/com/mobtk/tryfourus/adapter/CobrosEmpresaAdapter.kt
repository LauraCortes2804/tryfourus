package com.mobtk.tryfourus.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.databinding.ItemCobrosBinding
import com.mobtk.tryfourus.models.CobrosEmpresaAdminModel

class CobrosEmpresaAdapter(private  val items:List<CobrosEmpresaAdminModel>):
    RecyclerView.Adapter<CobrosEmpresaAdapter.ViewHolder>() {


    private var contexto: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto= parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCobrosBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CobrosEmpresaAdapter.ViewHolder, position: Int) {
        holder.bindi(items[position])

        if (items[position].tipo == 1){
            holder.databinding.iVStatus.setBackgroundResource(R.drawable.ic_mastercard)
        }else{
            holder.databinding.iVStatus.setBackgroundResource(R.drawable.ic_visa)
        }
    }

    class ViewHolder(val databinding :ItemCobrosBinding):RecyclerView.ViewHolder(databinding.root) {

        fun bindi(cobrosModel:CobrosEmpresaAdminModel){
            databinding.elemento = cobrosModel
            //cobrosModel.cantidad = "200"
            databinding.executePendingBindings()
            databinding.tvNotLeiNo
        }
    }
}