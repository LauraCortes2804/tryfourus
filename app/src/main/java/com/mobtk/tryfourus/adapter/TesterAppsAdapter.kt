package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.activities.AppCategoriasTesterActivity
import com.mobtk.tryfourus.databinding.ItemAppsTesterHomeBinding
import com.mobtk.tryfourus.databinding.ItemCategoriasHomeTesterBinding
import com.mobtk.tryfourus.models.AppsCompanyModel
import com.mobtk.tryfourus.models.CategoriasAppsModel

class TesterAppsAdapter (private val items: ArrayList<CategoriasAppsModel>): RecyclerView.Adapter<TesterAppsAdapter.ViewHolder>(){

    private var contexto: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCategoriasHomeTesterBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])

        if (items[position].nombreCategoriaApp == "Movilidad"){
            holder.binding.imgAplicaciones.setImageResource(R.drawable.didi)

        }
        if (items[position].nombreCategoriaApp == "Bancos"){
            holder.binding.imgAplicaciones.setImageResource(R.drawable.bank)
        }

        if (items[position].nombreCategoriaApp == "Redes Sociales"){
            holder.binding.imgAplicaciones.setImageResource(R.drawable.instagram)
        }

        if (items[position].nombreCategoriaApp == "Entretenimiento"){
            holder.binding.imgAplicaciones.setImageResource(R.drawable.play_juegos)
        }

        if (items[position].nombreCategoriaApp == "Educativas"){
            holder.binding.imgAplicaciones.setImageResource(R.drawable.usertesterayuda)
        }

        if (items[position].nombreCategoriaApp == "Destreza"){
            holder.binding.imgAplicaciones.setImageResource(R.drawable.solution)
        }

        holder.binding.setClickListener {
            when(it!!.id){
                holder.binding.cursosTesterHome.id ->{
                    val intent = Intent(holder.itemView.context, AppCategoriasTesterActivity::class.java)
                    contexto?.startActivity(intent)

                }
            }
        }
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val  binding: ItemCategoriasHomeTesterBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(appsCategoriasModel: CategoriasAppsModel){
            binding.elemento =appsCategoriasModel
            binding.executePendingBindings()
        }
    }
}