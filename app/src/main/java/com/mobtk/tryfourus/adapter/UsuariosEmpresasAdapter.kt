package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.activities.DetalleAdministradorUEmpresaActivity
import com.mobtk.tryfourus.databinding.ItemUsuariosEmpresasBinding
import com.mobtk.tryfourus.models.UsuariosEmpresaModel
import java.util.zip.Inflater

class UsuariosEmpresasAdapter(private val items: List<UsuariosEmpresaModel>) :
    RecyclerView.Adapter<UsuariosEmpresasAdapter.ViewHolder>() {
    private var contexto: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemUsuariosEmpresasBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])

        holder.binding.setClickListener {
            when (it!!.id) {
                holder.binding.itemEmpresa.id -> {
                    val intent = Intent(holder.itemView.context, DetalleAdministradorUEmpresaActivity::class.java)
                    intent.putExtra("nombre_empresa", items[position].nombreE)
                    holder.itemView.context.startActivity(intent)
                }
            }
        }
    }

    inner class ViewHolder(val binding: ItemUsuariosEmpresasBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(usuariosEmpresaModel: UsuariosEmpresaModel) {
            binding.usuarioEmpresa = usuariosEmpresaModel
            binding.executePendingBindings()
        }
    }

}