package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.activities.EmpresaDetalleActivity
import com.mobtk.tryfourus.models.AppsCompanyModel
import com.mobtk.tryfourus.databinding.ItemAplicacionBinding
import kotlinx.android.synthetic.main.fragment_profile.*

class CompanyAppsAdapter (private val items: ArrayList<AppsCompanyModel>): RecyclerView.Adapter<CompanyAppsAdapter.ViewHolder>(){

    companion object {
        val VER = "version"
        val NOMBRE = "nombre"
    }
    private var contexto: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemAplicacionBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        val appsCompanyModel = items.get(position)
        holder.binding.itemTvNombreAplicacion?.text = appsCompanyModel.name
        holder.binding.itemTvVersion?.text ="Version: "+ appsCompanyModel.version

        holder.binding.setClickListener {

            val intent = Intent(holder.binding.cvApps.context, EmpresaDetalleActivity::class.java)

            intent.putExtra(NOMBRE, appsCompanyModel.name)
            holder.itemView.context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val binding: ItemAplicacionBinding): RecyclerView.ViewHolder(binding.root){
       fun bind(appsCompanyModel: AppsCompanyModel){}
    }
}