package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.text.LoginFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.activities.LoginActivity
import com.mobtk.tryfourus.activities.MainActivity
import com.mobtk.tryfourus.databinding.ItemPagosBinding
import com.mobtk.tryfourus.models.PagosModel

class PagosAdapter(private val items: List<PagosModel>) : RecyclerView.Adapter<PagosAdapter.ViewHolder>() {
    private var contexto: Context? =null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto=parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemPagosBinding.inflate(inflater,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])

        if (items[position].pago_tarjeta=="1"){
         holder.binding.ivStatus.setImageDrawable(contexto?.resources?.getDrawable(R.drawable.ic_mastercard,contexto?.theme))
        }else{
            holder.binding.ivStatus.setImageDrawable(contexto?.resources?.getDrawable(R.drawable.ic_visa,contexto?.theme))
        }
    }

    inner class ViewHolder(val binding: ItemPagosBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(pagosModel: PagosModel) {
            binding.elemento= pagosModel
            binding.executePendingBindings()
        }
    }
}