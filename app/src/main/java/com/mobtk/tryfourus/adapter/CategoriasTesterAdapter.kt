package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.activities.DetalleAppActivity
import com.mobtk.tryfourus.databinding.ItemAppsTesterHomeBinding
import com.mobtk.tryfourus.models.AppsCompanyModel

class CategoriasTesterAdapter (private val item: List<AppsCompanyModel>): RecyclerView.Adapter<CategoriasTesterAdapter.ViewHolder>() {

    private var contexto: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        contexto=parent.context
        val binding = ItemAppsTesterHomeBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = item.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(item[position])
        if (item[position].categoria == "1") {
            holder.bindind.tVversionAplicacion.text = "Entretenimiento"
            holder.bindind.imgAplicaciones.setImageResource(R.drawable.didi)

        }
        if (item[position].categoria == "2") {
            holder.bindind.tVversionAplicacion.text  = "Viajes"
            holder.bindind.imgAplicaciones.setImageResource(R.drawable.uber_logo)
        }
        if (item[position].categoria == "3") {
            holder.bindind.tVversionAplicacion.text = "Juegos"
            holder.bindind.imgAplicaciones.setImageResource(R.drawable.frutas)
        }

        holder.bindind.setClickListener {
            when(it!!.id){
                holder.bindind.categorias.id ->{
                    val intent = Intent(contexto, DetalleAppActivity::class.java)
                    intent.putExtra("nombre", item[position].name)
                    contexto?.startActivity(intent)

                }
            }
        }

    }

    inner class ViewHolder(val bindind: ItemAppsTesterHomeBinding): RecyclerView.ViewHolder(bindind.root){
        fun bind(appModel: AppsCompanyModel){
            bindind.elemento = appModel
            bindind.executePendingBindings()
        }

    }
}