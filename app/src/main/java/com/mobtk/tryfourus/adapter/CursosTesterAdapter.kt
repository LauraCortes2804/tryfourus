package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.activities.AppCategoriasTesterActivity
import com.mobtk.tryfourus.activities.EmpresaDetalleActivity
import com.mobtk.tryfourus.activities.PreviewReport
import com.mobtk.tryfourus.databinding.ItemCursosTesterBinding
import com.mobtk.tryfourus.databinding.ItemCursosTesterHomeBinding
import com.mobtk.tryfourus.models.CursosModel

class CursosTesterAdapter (private val items: ArrayList<CursosModel>): RecyclerView.Adapter<CursosTesterAdapter.ViewHolder>(){

    private var contexto: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCursosTesterHomeBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])

        if (items[position].nivelC == "1") {
            holder.binding.imgAplicaciones.setImageResource(R.drawable.cursonivel1)

        }
        if (items[position].nivelC == "2") {
            holder.binding.imgAplicaciones.setImageResource(R.drawable.cursonivel2)
        }
        if (items[position].nivelC == "3") {
            holder.binding.imgAplicaciones.setImageResource(R.drawable.cursonivel3)
        }
        if (items[position].nivelC == "4") {
            holder.binding.imgAplicaciones.setImageResource(R.drawable.cursonivel4)
        }
        holder.binding.setClickListener {
            when(it!!.id){
                holder.binding.cursosTesterHome.id ->{
                    val intent = Intent(holder.itemView.context, PreviewReport::class.java)

                    contexto?.startActivity(intent)

                }
            }
        }

    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val binding: ItemCursosTesterHomeBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(cursoModel: CursosModel){
            binding.elemento = cursoModel
            binding.executePendingBindings()
        }
    }
}