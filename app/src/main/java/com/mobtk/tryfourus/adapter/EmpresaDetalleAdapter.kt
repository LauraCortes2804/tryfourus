package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.activities.DetalleAdministradorUTesterActivity
import com.mobtk.tryfourus.activities.ReporteBugsTesterActivity
import com.mobtk.tryfourus.databinding.ItemUsuariosTestersBinding
import com.mobtk.tryfourus.models.BugsTesterModel
import com.mobtk.tryfourus.models.UsuariosTesterModel

class EmpresaDetalleAdapter(private val items: List<UsuariosTesterModel>):RecyclerView.Adapter<EmpresaDetalleAdapter.ViewHolder>() {

    private var contexto: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemUsuariosTestersBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        holder.binding.setClickListener {
            when(it!!.id){
                holder.binding.itemUsuarioTester.id->{
                    val intent = Intent(holder.itemView.context, ReporteBugsTesterActivity::class.java)

                    holder.itemView.context.startActivity(intent)
                }
            }
        }
    }

    class ViewHolder(val binding: ItemUsuariosTestersBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(usuariosTesterModel: UsuariosTesterModel){
            binding.usuarioTester = usuariosTesterModel
            binding.executePendingBindings()
        }
    }

}