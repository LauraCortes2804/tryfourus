package com.mobtk.tryfourus.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.databinding.ItemCursosTesterPerfilBinding
import com.mobtk.tryfourus.models.CursosModel

class CursosTesterPerfilAdapter (private var items: List<CursosModel>) : RecyclerView.Adapter<CursosTesterPerfilAdapter.ViewHolder>() {
    private var contexto: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCursosTesterPerfilBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(val binding: ItemCursosTesterPerfilBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(cursosModel: CursosModel) {
            binding.elemento = cursosModel
            binding.executePendingBindings()
        }

    }
}