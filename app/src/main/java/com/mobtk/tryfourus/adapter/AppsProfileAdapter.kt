package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.activities.AppDetalleActivity
import com.mobtk.tryfourus.databinding.ItemAppsProfileBinding
import com.mobtk.tryfourus.models.AppsCompanyModel

class AppsProfileAdapter(private val items: ArrayList<AppsCompanyModel>): RecyclerView.Adapter<AppsProfileAdapter.ViewHolder>() {

    private var contexto: Context? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemAppsProfileBinding.inflate(inflater,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        val appsProfile = items.get(position)
        holder.binding.tvAppProfile?.text = appsProfile.name
        holder.binding.tvVersionApp?.text = "Version: "+appsProfile.version

        holder.binding.setClickListener {
            when(it!!.id) {
                holder.binding.cardViewAppsProfile.id -> {
                    val intent = Intent(holder.itemView.context, AppDetalleActivity::class.java)
                    intent.putExtra("nombre", items[position].name)
                    holder.itemView.context.startActivity(intent)
                }
            }
        }
    }

    class ViewHolder(val binding: ItemAppsProfileBinding):RecyclerView.ViewHolder(binding.root){
        fun bind (appsCompanyModel: AppsCompanyModel){}

    }
}