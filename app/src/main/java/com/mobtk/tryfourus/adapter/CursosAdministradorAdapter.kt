package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.activities.AgregarCursoAdministradorActivity
import com.mobtk.tryfourus.databinding.ItemCursosAdministradorBinding
import com.mobtk.tryfourus.models.CursosModel

class CursosAdministradorAdapter(private val items: List<CursosModel>):
    RecyclerView.Adapter<CursosAdministradorAdapter.ViewHolder>(){

    private var contexto :Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCursosAdministradorBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])

        if (items[position].nivelC == "1"){
            holder.binding.imgCursos.setImageResource(R.drawable.folder)
            holder.binding.tvNombreCurso.text = "Agregar Curso"

            holder.binding.setClickListener {
                when (it!!.id) {
                    holder.binding.itemCursosAdmin.id -> {
                        val intent = Intent(holder.itemView.context, AgregarCursoAdministradorActivity::class.java)
                        holder.itemView.context.startActivity(intent)
                    }
                }
            }

        }
        if (items[position].nivelC == "2"){
            holder.binding.imgCursos.setImageResource(R.drawable.optimization)
        }
        if (items[position].nivelC == "3"){
            holder.binding.imgCursos.setImageResource(R.drawable.review)
        }
        if (items[position].nivelC == "4"){
            holder.binding.imgCursos.setImageResource(R.drawable.ux)
        }

    }

    inner class ViewHolder(val binding: ItemCursosAdministradorBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(cursosModel: CursosModel){
            binding.elemento = cursosModel
            binding.executePendingBindings()
        }
    }
}