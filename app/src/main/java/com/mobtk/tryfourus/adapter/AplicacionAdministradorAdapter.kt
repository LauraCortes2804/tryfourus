package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.facebook.common.references.SharedReference
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.activities.AplicaionDetalleAdministradorActivity
import com.mobtk.tryfourus.databinding.ItemAplicacionAdministradorBinding
import com.mobtk.tryfourus.models.AppsCompanyModel
import com.mobtk.tryfourus.utilities.SharedPreference

class AplicacionAdministradorAdapter(private val items: List<AppsCompanyModel>) :
    RecyclerView.Adapter<AplicacionAdministradorAdapter.ViewHolder>() {

    private var context: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val binding = ItemAplicacionAdministradorBinding.inflate(inflater, parent, false)
        return  ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bing(items.get(position))
        if(items[position].categoria == "1"){
            holder.binding.imgAplicaciones.setImageResource(R.drawable.uber_logo)
        }
        if(items[position].categoria == "3"){
            holder.binding.imgAplicaciones.setImageResource(R.drawable.frutas)
        }
        if(items[position].categoria == "2"){
            holder.binding.imgAplicaciones.setImageResource(R.drawable.didi)
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(context, AplicaionDetalleAdministradorActivity::class.java)
            intent.putExtra("DatosAplicaion", items[position] )
            context?.startActivity(intent)
        }
    }

    class ViewHolder(val binding :ItemAplicacionAdministradorBinding):RecyclerView.ViewHolder(binding.root) {
        fun bing(appModel:AppsCompanyModel){
            binding.elemento = appModel
            binding.executePendingBindings()
        }
    }
}