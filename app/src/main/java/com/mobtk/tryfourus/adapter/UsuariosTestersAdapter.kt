package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.activities.DetalleAdministradorUTesterActivity
import com.mobtk.tryfourus.databinding.ItemUsuariosTestersBinding
import com.mobtk.tryfourus.models.UsuariosTesterModel
import com.mobtk.tryfourus.utilities.DynamicSearchAdapter

class UsuariosTestersAdapter(private val items:MutableList<UsuariosTesterModel>)
    : DynamicSearchAdapter<UsuariosTesterModel>(items) {
    private var contexto: Context? = null
    lateinit var binding: ItemUsuariosTestersBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemUsuariosTestersBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(val binding: ItemUsuariosTestersBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(usuariosTesterModel: UsuariosTesterModel){
            binding.usuarioTester = usuariosTesterModel
            binding.executePendingBindings()
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        binding.setClickListener{
            when(it!!.id){
                binding.itemUsuarioTester.id->{
                    val intent = Intent(holder.itemView.context, DetalleAdministradorUTesterActivity::class.java)
                    intent.putExtra("nombre_tester", items[position].nombreT)
                    intent.putExtra("apellidos_tester", items[position].apellidosT)
                    holder.itemView.context.startActivity(intent)
                }
            }
        }
    }

}