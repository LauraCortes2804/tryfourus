package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.activities.DetalleAppAdministradorActivity
import com.mobtk.tryfourus.databinding.ItemAppsAdministradorBinding
import com.mobtk.tryfourus.models.AppsCompanyModel

class AppsAdministradorAdapter(private var items: List<AppsCompanyModel>) :
    RecyclerView.Adapter<AppsAdministradorAdapter.ViewHolder>() {
    private var contexto: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemAppsAdministradorBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])

        if (items[position].categoria == "1") {
            holder.binding.tvNombreCategoria.text = "Entretenimiento"
        }
        if (items[position].categoria == "2") {
            holder.binding.tvNombreCategoria.text  = "Viajes"
        }
        if (items[position].categoria == "3") {
            holder.binding.tvNombreCategoria.text = "Juegos"
        }
        holder.binding.setClickListener {
            when (it!!.id) {
                holder.binding.itemAppsAdmin.id -> {
                    val intent = Intent(holder.itemView.context, DetalleAppAdministradorActivity::class.java)
                    intent.putExtra("nombre_app", items[position].name)
                    holder.itemView.context.startActivity(intent)
                }
            }
        }
    }

    inner class ViewHolder(val binding: ItemAppsAdministradorBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(appsCompanyModel: AppsCompanyModel) {
            binding.appsAdmin = appsCompanyModel
            binding.executePendingBindings()
        }

    }
}