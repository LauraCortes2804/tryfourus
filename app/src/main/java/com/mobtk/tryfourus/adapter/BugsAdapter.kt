package com.mobtk.tryfourus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.activities.ReporteBugsTesterActivity
import com.mobtk.tryfourus.databinding.ItemAppBugsBinding
import com.mobtk.tryfourus.models.BugsTesterModel
import com.mobtk.tryfourus.utilities.SharedPreference

class BugsAdapter (private val item: List<BugsTesterModel>): RecyclerView.Adapter<BugsAdapter.ViewHolder> () {
    private lateinit var contexto: Context

    inner class ViewHolder (val binding: ItemAppBugsBinding): RecyclerView.ViewHolder(binding.root){ // 2. debemos indicar donde se renderizara el modelo
        fun bind(bugTesterModel: BugsTesterModel){

            binding.elemento =bugTesterModel
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {  // 1. primero devemos indicar donde inflaremos la vista
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemAppBugsBinding.inflate(inflater,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = item.size  // indicar que regrese el tamaño de la lista

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(item[position])
        holder.binding.setClickListener {
            when(it!!.id){
                holder.binding.cardViewBug.id ->{
                    val intent = Intent(holder.itemView.context, ReporteBugsTesterActivity::class.java)
                    val sharedPreference= SharedPreference(contexto)
                    var tipousuario= sharedPreference.getValueInt("bugs")
                    intent.putExtra("bugs",tipousuario)
                    intent.putExtra("bug_nombre", item[position].nombreBug)
                    intent.putExtra("bug_reportados", item[position].bugReportados)
                    intent.putExtra("bug_aprobados", item[position].bugAprobados)
                    intent.putExtra("bug_desc", item[position].bugDesc)
                    intent.putExtra("bug_cat", item[position].bugCatego)
                    contexto.startActivity(intent)

                }
            }
        }
    }
}