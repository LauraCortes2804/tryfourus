package com.mobtk.tryfourus.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.databinding.ItemNotificacionesBinding
import com.mobtk.tryfourus.databinding.ItemPagosBinding
import com.mobtk.tryfourus.models.NotificacionModel
import com.mobtk.tryfourus.models.PagosModel

class NotificacionAdapter(private val items: List<NotificacionModel>) : RecyclerView.Adapter<NotificacionAdapter.ViewHolder>() {
    private var contexto: Context? =null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto=parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemNotificacionesBinding.inflate(inflater,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(val binding: ItemNotificacionesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(notificacionModel: NotificacionModel) {
            binding.elemento= notificacionModel
            binding.executePendingBindings()
        }
    }
}