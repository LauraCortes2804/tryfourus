package com.mobtk.tryfourus.fragments

import android.content.Intent
import br.com.ilhasoft.support.validation.Validator
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.databinding.FragmentSubirAppsBinding

class SubirAppsFragment : Fragment(), Validator.ValidationListener {
    private val apk = 18
    private lateinit var binding:FragmentSubirAppsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_subir_apps, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val validator = Validator(binding)
        validator.setValidationListener(this)
        binding.clickListener = View.OnClickListener {
            when(it!!.id) {
                binding.btnSaveApp.id -> {
                    validator!!.toValidate()
                }

                binding.btnCargarArchivo.id -> {
                    val intent = Intent(Intent.ACTION_GET_CONTENT)
                    intent.type = "application/vnd.android.package-archive"
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
                    startActivityForResult(
                        Intent.createChooser(
                            intent,
                            getString(R.string.seleccion)
                        ), apk
                    )
                }
            }
        }
    }

    override fun onValidationError() {
    }

    override fun onValidationSuccess() {
    }
}

