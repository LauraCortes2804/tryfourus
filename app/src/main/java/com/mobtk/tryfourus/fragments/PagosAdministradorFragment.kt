package com.mobtk.tryfourus.fragments


import android.os.Bundle
import kotlinx.android.synthetic.main.fragment_pagos_administrador.*
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.mobtk.tryfourus.R

/**
 * A simple [Fragment] subclass.
 */
class PagosAdministradorFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pagos_administrador, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = MyViewPagerAdapter(childFragmentManager)
        adapter.addFragment(PagosTesterAdministradorFragment(), "Pagos")
        adapter.addFragment(CobrosEmpresaAdministradorFragment(),"Cobros")
        viewPagers.adapter = adapter
        tabs.setupWithViewPager(viewPagers)
    }

    class MyViewPagerAdapter(manager : FragmentManager): FragmentPagerAdapter(manager){
        private  val fragmenList : MutableList<Fragment> = ArrayList()
        private  val lisTitulo : MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmenList[position]
        }


        override fun getCount(): Int {
            return fragmenList.size
        }

        fun agregarFragment(fragment:Fragment, titulo:String){
            fragmenList.add(fragment)
            lisTitulo.add(titulo)
        }

        fun addFragment(fragment: Fragment, title:String){
            fragmenList.add(fragment)
            lisTitulo.add(title)

        }

        override fun getPageTitle(position: Int): CharSequence? {
            return lisTitulo[position]
        }
    }


}
