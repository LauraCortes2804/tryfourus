package com.mobtk.tryfourus.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.UsuariosEmpresasAdapter
import com.mobtk.tryfourus.databinding.FragmentAdministradorUempresasBinding
import com.mobtk.tryfourus.models.UsuariosEmpresaModel
import com.mobtk.tryfourus.task.EmpresaTask
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class AdministradorUEmpresasFragment : Fragment() {
    private lateinit var binding: FragmentAdministradorUempresasBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var empresas:List<UsuariosEmpresaModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_administrador_uempresas, container, false
        )

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        empresas = ArrayList()
        getEmpresas()
    }

    private fun getEmpresas() {
        doAsync {
            val response = EmpresaTask.getEmpresas(getString(R.string.base_url))
            uiThread {
                try {
                    linearLayoutManager = LinearLayoutManager(context)
                    binding.RecyclerViewEmpresas.layoutManager = linearLayoutManager
                    empresas = response.usuariosEmpresa
                    binding.RecyclerViewEmpresas.adapter = UsuariosEmpresasAdapter(empresas)
                    avi.hide()
                    carga.visibility=View.GONE

                }catch (ex:Exception){

                }

            }
        }
    }

}
