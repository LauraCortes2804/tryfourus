package com.mobtk.tryfourus.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.BugsAdapter
import com.mobtk.tryfourus.databinding.FragmentBugsBinding
import com.mobtk.tryfourus.models.BugsTesterModel
import com.mobtk.tryfourus.task.BugsTask
import kotlinx.android.synthetic.main.fragment_bugs.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception


class BugsFragment : Fragment(){

    private lateinit var binding: FragmentBugsBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var bugReporte: List<BugsTesterModel>
    private var animation : LayoutAnimationController? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_bugs, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(binding.root, savedInstanceState)

        animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)
        binding.rvBugReporte.setLayoutAnimation(animation)
        swipe_refresh.setOnRefreshListener {
            bugsReportes()
        }
        bugReporte = ArrayList()
        bugsReportes()
    }

    private fun bugsReportes(){
        doAsync {
            val response = BugsTask.getBugs(getString(R.string.base_url))
            uiThread {

                try {
                    linearLayoutManager = LinearLayoutManager(context)
                    binding.rvBugReporte.layoutManager = linearLayoutManager
                    bugReporte=response.reporteBug
                    binding.rvBugReporte.adapter = BugsAdapter(bugReporte)
                    avi.hide()
                    carga.visibility=View.GONE
                    swipe_refresh.isRefreshing = false
                }catch (ex:Exception){

                }
            }
        }
    }
}