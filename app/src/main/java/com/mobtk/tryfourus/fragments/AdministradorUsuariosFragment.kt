package com.mobtk.tryfourus.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.utilities.ViewPageAdapter
import kotlinx.android.synthetic.main.fragment_administrador_usuarios.*

/**
 * A simple [Fragment] subclass.
 */
class AdministradorUsuariosFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_administrador_usuarios, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val adapterA = ViewPageAdapter(childFragmentManager)
        adapterA.addFragment(AdministradorUTesterFragment(), "USUARIO")
        adapterA.addFragment(AdministradorUEmpresasFragment(), "EMPRESA")
        viewPagerF.adapter = adapterA
        tabsF.setupWithViewPager(viewPagerF)
    }

}
