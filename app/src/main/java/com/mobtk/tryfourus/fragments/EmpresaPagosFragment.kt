package com.mobtk.tryfourus.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.ilhasoft.support.validation.Validator

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.PagosAdapter
import com.mobtk.tryfourus.databinding.FragmentCompanyBinding
import com.mobtk.tryfourus.databinding.FragmentEmpresaPagosBinding
import com.mobtk.tryfourus.models.PagosModel
import com.mobtk.tryfourus.task.PagosTask
import kotlinx.android.synthetic.main.fragment_empresa_pagos.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

class EmpresaPagosFragment : Fragment() {
    private lateinit var binding: FragmentEmpresaPagosBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var animation : LayoutAnimationController? = null
    lateinit var pagos:List<PagosModel>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_empresa_pagos, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(binding.root, savedInstanceState)

        swipe_refresh.setOnRefreshListener {
            getPagosWS()
        }
        pagos=ArrayList()
        getPagosWS()
    }

    private fun getPagosWS() {
        doAsync {
            val message = PagosTask.getPagos(getString(R.string.base_url))
            uiThread {
                try {
                    animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)
                    binding.rvPagos.layoutAnimation=animation
                    linearLayoutManager = LinearLayoutManager(context)
                    binding.rvPagos.layoutManager = linearLayoutManager
                    pagos=message.pagos
                    binding.rvPagos.adapter = PagosAdapter(pagos)
                    binding.rvPagos.scheduleLayoutAnimation()
                    avi.hide()
                    carga.visibility=View.GONE
                    swipe_refresh.isRefreshing = false
                }catch (ex:Exception){

                }
            }
        }
    }
}

