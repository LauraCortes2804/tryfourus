package com.mobtk.tryfourus.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.CobrosEmpresaAdapter
import com.mobtk.tryfourus.databinding.FragmentCobrosEmpresaAdministradorBinding
import com.mobtk.tryfourus.task.CobrosEmpresaAdministradorTask
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class CobrosEmpresaAdministradorFragment : Fragment() {

    private lateinit var linearLayoutManager:LinearLayoutManager
    private lateinit var binding:FragmentCobrosEmpresaAdministradorBinding
    private var animation : LayoutAnimationController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cobros_empresa_administrador, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)
        binding.rvCobrosEmpresaAdmin.setLayoutAnimation(animation)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getCobros()
    }


    fun getCobros() {
        doAsync {
            val call = CobrosEmpresaAdministradorTask.getCobrosEmpresaAdmin(getString(R.string.base_url))
            Log.d("TAG", "Response "+call.cobros)
            uiThread {
                try {
                    linearLayoutManager = LinearLayoutManager(context)
                    binding.rvCobrosEmpresaAdmin.layoutManager = linearLayoutManager
                    binding.rvCobrosEmpresaAdmin.adapter = CobrosEmpresaAdapter(call.cobros)
                    avi.hide()
                    carga.visibility=View.GONE

                }catch (ex:Exception){

                }

            }
        }
    }


}
