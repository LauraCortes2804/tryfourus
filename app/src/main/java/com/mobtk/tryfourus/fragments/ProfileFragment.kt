package com.mobtk.tryfourus.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LayoutAnimationController
import com.bumptech.glide.Glide

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.utilities.SharedPreference
import com.mobtk.tryfourus.utilities.ViewPageAdapter
import kotlinx.android.synthetic.main.fragement_notificaciones.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.swipe_refresh

class ProfileFragment : Fragment() {
    var contexto: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPreference=SharedPreference(getActivity()!!.getApplicationContext())
        var banderaParma= sharedPreference.getValueInt("usuario")
        Log.d("TAG", "DatoLogin " + banderaParma)

        iv_profileEmpresa.setImageResource(R.drawable.ic_launcher_background)
        tvNameProfileEmpresa.setText(R.string.name_company)

        val adapter = ViewPageAdapter(childFragmentManager)
        adapter.addFragment(AppsProfileFragment(), "Aplicaciones")
        adapter.addFragment(BugsFragment(), "Reportes")
        adapter.addFragment(EmpresaPagosFragment(), "Pagos")
        viewPagerE.adapter = adapter
        tabsUEmpresa.setupWithViewPager(viewPagerE)

        if(banderaParma == 1){
            iv_profileEmpresa.setImageResource(R.drawable.usuarios)
            tvNameProfileEmpresa.setText(R.string.name_admin)

            val adapter = ViewPageAdapter(childFragmentManager)
            adapter.addFragment(AppsProfileAdminFragment(), "Aplicaciones")
            adapter.addFragment(BugsFragment(), "Historial")
            viewPagerE.adapter = adapter
            tabsUEmpresa.setupWithViewPager(viewPagerE)

        }else if(banderaParma == 2){
            swipe_refresh.setOnRefreshListener {

                setEmpresaProfile()
            }
           setEmpresaProfile()

        }else if (banderaParma == 3){
            iv_profileEmpresa.setImageResource(R.drawable.usuarios)
            tvNameProfileEmpresa.setText(R.string.name_tester)

            val adapter = ViewPageAdapter(childFragmentManager)
            adapter.addFragment(AppsProfileAdminFragment(), "Aplicaciones")
            adapter.addFragment(CursosPerfilTesterFragment(), "Cursos")
            adapter.addFragment(BugsFragment(), "Historial")
            viewPagerE.adapter = adapter
            tabsUEmpresa.setupWithViewPager(viewPagerE)
        }
    }
    fun setEmpresaProfile(){
        val requestManager = Glide.with(this)
        val requestBuilder = requestManager.load("https://d3i4yxtzktqr9n.cloudfront.net/uber-sites/f452c7aefd72a6f52b36705c8015464e.jpg")
        requestBuilder.into(iv_profileEmpresa)
        tvNameProfileEmpresa.setText(R.string.name_company)

        val adapter = ViewPageAdapter(childFragmentManager)
        adapter.addFragment(AppsProfileFragment(), "Aplicaciones")
        adapter.addFragment(BugsFragment(), "Reportes")
        adapter.addFragment(EmpresaPagosFragment(), "Pagos")
        viewPagerE.adapter = adapter
        tabsUEmpresa.setupWithViewPager(viewPagerE)
        swipe_refresh.isRefreshing = false
    }
}