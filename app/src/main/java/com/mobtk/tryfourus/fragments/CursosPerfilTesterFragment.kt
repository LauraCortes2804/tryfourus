package com.mobtk.tryfourus.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.CursosTesterPerfilAdapter
import com.mobtk.tryfourus.models.CursosModel
import com.mobtk.tryfourus.task.CursosTask
import kotlinx.android.synthetic.main.fragment_cursos_perfil_tester.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class CursosPerfilTesterFragment : Fragment() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var cursosTester: ArrayList<CursosModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cursos_perfil_tester, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        cursosTester = ArrayList()
        cursosoTesterPerfil()

    }

    private fun cursosoTesterPerfil(){
        doAsync {
            val cursoPerfil = CursosTask.getCursos(getString(R.string.base_url))
            uiThread {
                try {
                    linearLayoutManager = LinearLayoutManager(context)
                    rvCursosTesterPerfil.layoutManager = linearLayoutManager
                    cursosTester = cursoPerfil.cursosL
                    rvCursosTesterPerfil.adapter = CursosTesterPerfilAdapter(cursosTester)

                } catch (ex:Exception){

                }

            }
        }
    }


}
