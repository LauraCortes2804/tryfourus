package com.mobtk.tryfourus.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.PreguntasAdapter
import com.mobtk.tryfourus.databinding.FragmentPreguntasFrecuentesBinding
import com.mobtk.tryfourus.models.PreguntaModel
import com.mobtk.tryfourus.task.PreguntasTask
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class PreguntasFrecuentesFragment : Fragment() {
    private lateinit var binding: FragmentPreguntasFrecuentesBinding
    private lateinit var linearLayoutManager: LinearLayoutManager

    private var animation : LayoutAnimationController? = null
    lateinit var preguntas:ArrayList<PreguntaModel>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_preguntas_frecuentes, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(binding.root, savedInstanceState)

        getNotificacionesWS()
    }

    private fun getNotificacionesWS() {
        doAsync {
            val message = PreguntasTask.getPreguntas(getString(R.string.base_url))
            uiThread {
                animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)
                binding.rvPreguntas.layoutAnimation=animation
                linearLayoutManager = LinearLayoutManager(context)
                binding.rvPreguntas.layoutManager = linearLayoutManager
                preguntas=message.preguntas
                binding.rvPreguntas.adapter = PreguntasAdapter(preguntas)
                binding.rvPreguntas.scheduleLayoutAnimation()
                avi.hide()
                carga.visibility=View.GONE
            }
        }
    }
}