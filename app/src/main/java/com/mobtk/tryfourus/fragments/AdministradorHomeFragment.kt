package com.mobtk.tryfourus.fragments


import android.icu.lang.UCharacter
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.AplicacionAdministradorAdapter
import com.mobtk.tryfourus.adapter.CursosAdministradorAdapter
import com.mobtk.tryfourus.databinding.FragmentAdministradorHomeBinding
import com.mobtk.tryfourus.models.AppsCompanyModel
import com.mobtk.tryfourus.models.CursosModel
import com.mobtk.tryfourus.models.UsuariosEmpresaModel
import com.mobtk.tryfourus.task.AppTask
import com.mobtk.tryfourus.task.CursosTask
import kotlinx.android.synthetic.main.carga.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class AdministradorHomeFragment : Fragment() {
    private lateinit var binding: FragmentAdministradorHomeBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var appsLista: List<AppsCompanyModel>
    lateinit var cursosLista: List<CursosModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_administrador_home,
            container,
            false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        cursosLista = ArrayList()
        getCursos()
        appsLista = ArrayList()
        getApp()
    }

    private fun getCursos() {
        doAsync {
            val response = CursosTask.getCursos(getString(R.string.base_url))
            uiThread {
                try {
                    linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false )
                    binding.rvCursosAdministrador.layoutManager = linearLayoutManager
                    cursosLista = response.cursosL
                    binding.rvCursosAdministrador.adapter = CursosAdministradorAdapter(cursosLista)
                    var layaout1 = view?.findViewById<RelativeLayout>(R.id.primerAdministradorRV)
                    layaout1?.aviPac?.hide()
                    layaout1?.cargaRv?.visibility = View.GONE
                    layaout1?.visibility=View.GONE

                } catch (ex:Exception){

                }

            }
        }
    }

    private fun getApp() {
        doAsync {
            val response = AppTask.getApp(getString(R.string.base_url))
            uiThread {
                try {
                    linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false )
                    binding.rvAplicacionesAdministrador.layoutManager = linearLayoutManager
                    appsLista = response.aplica
                    binding.rvAplicacionesAdministrador.adapter = AplicacionAdministradorAdapter(appsLista)
                    var layaout1 = view?.findViewById<RelativeLayout>(R.id.segundoAdministradorRV)
                    layaout1?.aviPac?.hide()
                    layaout1?.cargaRv?.visibility = View.GONE
                    layaout1?.visibility=View.GONE

                }catch (ex:Exception){

                }

            }
        }
    }


}
