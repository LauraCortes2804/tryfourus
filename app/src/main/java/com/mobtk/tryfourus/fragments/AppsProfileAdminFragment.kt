package com.mobtk.tryfourus.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.AppsAdministradorAdapter
import com.mobtk.tryfourus.models.AppsCompanyModel
import com.mobtk.tryfourus.task.AppTask
import kotlinx.android.synthetic.main.fragment_apps_profile_admin.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class AppsProfileAdminFragment : Fragment() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var appsAdmin: ArrayList<AppsCompanyModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_apps_profile_admin, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        appsAdmin = ArrayList()
        appsProfileAdmin()
    }

    private fun appsProfileAdmin(){
        doAsync {
            val appProfile = AppTask.getApp(getString(R.string.base_url))
            uiThread {
                try {
                    linearLayoutManager = LinearLayoutManager(context)
                    rvProfileAppsAdmin.layoutManager = linearLayoutManager
                    appsAdmin = appProfile.aplica
                    rvProfileAppsAdmin.adapter = AppsAdministradorAdapter(appsAdmin)
                    avi.hide()
                    carga.visibility=View.GONE
                } catch (ex:Exception){

                }

            }
        }
    }

}
