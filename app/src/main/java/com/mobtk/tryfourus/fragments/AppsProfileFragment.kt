package com.mobtk.tryfourus.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.LinearLayoutManager

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.AppsProfileAdapter
import com.mobtk.tryfourus.models.AppsCompanyModel
import com.mobtk.tryfourus.task.AppTask
import kotlinx.android.synthetic.main.fragment_apps_profile.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * A simple [Fragment] subclass.
 */
class AppsProfileFragment : Fragment() {

    private lateinit var linearLayoutManager: LinearLayoutManager

    private var animation : LayoutAnimationController? = null
    lateinit var apps1: ArrayList<AppsCompanyModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_apps_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipe_refresh.setOnRefreshListener {
            appsProfile()
        }
        apps1 = ArrayList()
        appsProfile()
    }

    private fun appsProfile(){
        doAsync {
            val appProfile = AppTask.getApp(getString(R.string.base_url))
            uiThread {
                apps1 = appProfile.aplica
                try{
                    animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)
                    rvProfileApps.layoutAnimation=animation
                linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                rvProfileApps.layoutManager = linearLayoutManager
                rvProfileApps.adapter = AppsProfileAdapter(apps1)
                    rvProfileApps.scheduleLayoutAnimation()
                    avi.hide()
                    carga.visibility=View.GONE
                }catch (ex:Exception){


                }
            }
        }
    }
}