package com.mobtk.tryfourus.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LayoutAnimationController
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.animation.AnimationUtils

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.PagosAdapter
import com.mobtk.tryfourus.databinding.FragmentPagosEmpresaAdministradorBinding
import com.mobtk.tryfourus.task.PagosTask
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class PagosTesterAdministradorFragment : Fragment() {

    private lateinit var binding: FragmentPagosEmpresaAdministradorBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var animation :LayoutAnimationController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pagos_empresa_administrador, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)
        binding.rvPagosEmpresaAdmin.setLayoutAnimation(animation)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getPagos()
    }

    private fun getPagos() {
        doAsync {
            val call =
                PagosTask.getPagos(getString(R.string.base_url))
            uiThread {
                try {
                    linearLayoutManager = LinearLayoutManager(context)
                    binding.rvPagosEmpresaAdmin.layoutManager = linearLayoutManager
                    binding.rvPagosEmpresaAdmin.adapter = PagosAdapter(call.pagos)
                    avi.hide()
                    carga.visibility=View.GONE

                }catch (ex:Exception){

                }

            }
        }
    }


}
