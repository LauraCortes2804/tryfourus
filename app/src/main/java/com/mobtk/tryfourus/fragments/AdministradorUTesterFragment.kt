package com.mobtk.tryfourus.fragments


import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.UsuariosTestersAdapter
import com.mobtk.tryfourus.databinding.FragmentAdministradorUtesterBinding
import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.models.UsuariosTesterModel
import com.mobtk.tryfourus.responses.UsuariosTestersResponse
import com.mobtk.tryfourus.task.TesterTask
import com.mobtk.tryfourus.utilities.RetrofitConstructor
import kotlinx.android.synthetic.main.fragment_administrador_utester.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception


/**
 * A simple [Fragment] subclass.
 */
class AdministradorUTesterFragment : Fragment(), SearchView.OnQueryTextListener {

    private lateinit var binding: FragmentAdministradorUtesterBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var testers:MutableList<UsuariosTesterModel>

    private lateinit var searchAdapter3:UsuariosTestersAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_administrador_utester, container, false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        searchTester.setOnQueryTextListener(this)
        testers = ArrayList()
        getTester()
    }

    private fun getTester() {
        doAsync {
                val response = TesterTask.getTesters(getString(R.string.base_url))
            uiThread {
                try {
                    linearLayoutManager = LinearLayoutManager(context)
                    binding.RecyclerViewTesters.layoutManager = linearLayoutManager
                    testers = response.usuariosTester
                    searchAdapter3= UsuariosTestersAdapter(testers)
                    binding.RecyclerViewTesters.adapter = searchAdapter3
                    avi.hide()
                    carga.visibility=View.GONE

                }catch (ex:Exception){

                }


            }
        }
    }

    /**
     * Called when the user submits the query. This could be due to a key press on the
     * keyboard or due to pressing a submit button.
     * The listener can override the standard behavior by returning true
     * to indicate that it has handled the submit request. Otherwise return false to
     * let the SearchView handle the submission by launching any associated intent.
     *
     * @param query the query text that is to be submitted
     *
     * @return true if the query has been handled by the listener, false to let the
     * SearchView perform the default action.
     */
    override fun onQueryTextSubmit(query: String?): Boolean {
        search(query?.toLowerCase())
        return true
    }

    /**
     * Called when the query text is changed by the user.
     *
     * @param newText the new content of the query text field.
     *
     * @return false if the SearchView should perform the default action of showing any
     * suggestions if available, true if the action was handled by the listener.
     */
    override fun onQueryTextChange(newText: String?): Boolean {
        search(newText?.toLowerCase())
        return true
     }
    private fun search(s: String?) {
        searchAdapter3.search(s?.toLowerCase()) {
            // update UI on nothing found

        }
    }
}
