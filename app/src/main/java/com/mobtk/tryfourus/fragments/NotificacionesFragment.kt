package com.mobtk.tryfourus.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.NotificacionAdapter
import com.mobtk.tryfourus.databinding.FragementNotificacionesBinding
import com.mobtk.tryfourus.models.NotificacionModel
import com.mobtk.tryfourus.task.NotificationTask
import kotlinx.android.synthetic.main.fragement_notificaciones.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

class NotificacionesFragment : Fragment() {
    private lateinit var binding: FragementNotificacionesBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var notificaciones:List<NotificacionModel>

    private var animation : LayoutAnimationController? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragement_notificaciones, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(binding.root, savedInstanceState)
        swipe_refresh.setOnRefreshListener {
            getNotificacionesWS()
            swipe_refresh.isRefreshing = false
        }
        notificaciones=ArrayList()
        getNotificacionesWS()
    }

    private fun getNotificacionesWS() {
        doAsync {
            val message = NotificationTask.getNotificaciones(getString(R.string.base_url))
            uiThread {
                try {
                    animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)
                    binding.rvNotificaciones.layoutAnimation=animation
                    linearLayoutManager = LinearLayoutManager(context)
                    binding.rvNotificaciones.layoutManager = linearLayoutManager
                    notificaciones=message.notificacione
                    binding.rvNotificaciones.adapter = NotificacionAdapter(notificaciones)
                    binding.rvNotificaciones.scheduleLayoutAnimation()
                    avi.hide()
                    carga.visibility=View.GONE
                } catch (ex:Exception){

                }

            }
        }
    }
}