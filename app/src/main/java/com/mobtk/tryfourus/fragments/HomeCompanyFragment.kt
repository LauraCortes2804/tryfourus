package com.mobtk.tryfourus.fragments

import android.os.Bundle
import android.text.Layout
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobtk.tryfourus.models.AppsCompanyModel
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.CompanyAppsAdapter
import com.mobtk.tryfourus.task.AppTask
import kotlinx.android.synthetic.main.carga.*
import kotlinx.android.synthetic.main.carga.view.*
import kotlinx.android.synthetic.main.fragment_bugs.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.swipe_refresh
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class HomeCompanyFragment : Fragment() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var apps: ArrayList<AppsCompanyModel>
    lateinit var appsTrip: ArrayList<AppsCompanyModel>
    lateinit var appsSocial: ArrayList<AppsCompanyModel>
    private var animation : LayoutAnimationController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        apps = ArrayList()
        appsTrip = ArrayList()
        appsSocial = ArrayList()
        swipe_refresh.setOnRefreshListener {
            apps()
            appsTr()
            appsSocial()
        }
        apps()
        appsTr()
        appsSocial()
    }

    private fun apps(){
        doAsync {
          val app =  AppTask.getApp(getString(R.string.base_url))
            uiThread {
                try{
                    animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layaout_animation_horizontal)
                    rvCompanyNav.layoutAnimation=animation
                linearLayoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false)
                rvCompanyNav.layoutManager = linearLayoutManager
                apps=app.aplica
                rvCompanyNav.adapter = CompanyAppsAdapter(apps)
                    rvCompanyNav.scheduleLayoutAnimation()
                var layaout1 = view?.findViewById<RelativeLayout>(R.id.primerRV)
                layaout1?.aviPac?.hide()
                layaout1?.cargaRv?.visibility = View.GONE}
                catch (ex:Exception){

                }
            }
        }
    }

    private fun appsTr(){
        doAsync {
            val appSocial = AppTask.getApp(getString(R.string.base_url))
            uiThread {
                try{
                    animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layaout_animation_horizontal)
                    rvCompanySocial.layoutAnimation=animation
                linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                rvCompanySocial.layoutManager = linearLayoutManager
                appsSocial = appSocial.aplica
                rvCompanySocial.adapter = CompanyAppsAdapter(appsSocial)
                    rvCompanySocial.scheduleLayoutAnimation()
                var layaout2 = view?.findViewById<RelativeLayout>(R.id.segundoRv)
                layaout2?.aviPac?.hide()
                layaout2?.cargaRv?.visibility = View.GONE}
                catch (ex:Exception){

                }
            }
        }
    }

    private fun appsSocial(){
        doAsync {
            val appTrip = AppTask.getApp(getString(R.string.base_url))
            uiThread {
                try {
                    animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layaout_animation_horizontal)
                    rvCompanyTrip.layoutAnimation=animation
                    linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    rvCompanyTrip.layoutManager = linearLayoutManager
                    appsTrip = appTrip.aplica
                    rvCompanyTrip.adapter = CompanyAppsAdapter(appsTrip)
                    var layaout3 = view?.findViewById<RelativeLayout>(R.id.tercerRV)
                    rvCompanyTrip.scheduleLayoutAnimation()
                    layaout3?.aviPac?.hide()
                    layaout3?.cargaRv?.visibility = View.GONE
                    swipe_refresh.isRefreshing = false
                }catch (ex:Exception){

                }
            }
        }
    }
}