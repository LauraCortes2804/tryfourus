package com.mobtk.tryfourus.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager


import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.TesterAppsAdapter
import com.mobtk.tryfourus.adapter.CursosTesterAdapter
import com.mobtk.tryfourus.models.AppsCompanyModel
import com.mobtk.tryfourus.models.CategoriasAppsModel
import com.mobtk.tryfourus.models.CursosModel
import com.mobtk.tryfourus.task.AppTask
import com.mobtk.tryfourus.task.CategoriasTask
import com.mobtk.tryfourus.task.CursosTask
import kotlinx.android.synthetic.main.carga.view.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home_tester.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

//rvTesterCursos
class HomeTesterFragment : Fragment() {

    private lateinit var linearLayoutManager: LinearLayoutManager

    //models
    lateinit var cursos: ArrayList<CursosModel>
    lateinit var apps: ArrayList<CategoriasAppsModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_tester, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        cursos = ArrayList()
        Cursos()
        apps()
    }

    private fun Cursos(){
        doAsync {
            val call =  CursosTask.getCursos(getString(R.string.base_url))
            uiThread {
                try {
                    linearLayoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false)
                    rvTesterCursos.layoutManager = linearLayoutManager
                    cursos = call.cursosL
                    rvTesterCursos.adapter = CursosTesterAdapter(cursos)
                    var layaout1 = view?.findViewById<RelativeLayout>(R.id.primerTesterRV)
                    layaout1?.aviPac?.hide()
                    layaout1?.visibility = View.GONE

                }catch (ex:Exception){

                }

            }
        }
    }

    private fun apps(){
        doAsync {
            val app =  CategoriasTask.getCategoriasApps(getString(R.string.base_url))
            uiThread {
                try {
                    linearLayoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false)
                    rvCompanyEntertaT.layoutManager = linearLayoutManager
                    apps= app.categoriasApps
                    rvCompanyEntertaT.adapter = TesterAppsAdapter(apps)
                    var layaout1 = view?.findViewById<RelativeLayout>(R.id.segundoTesterRV)
                    layaout1?.aviPac?.hide()
                    layaout1?.visibility = View.GONE

                }catch (ex:Exception){

                }

            }
        }
    }
}