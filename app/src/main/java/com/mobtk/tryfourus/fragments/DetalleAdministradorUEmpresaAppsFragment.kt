package com.mobtk.tryfourus.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.adapter.AppsAdministradorAdapter
import com.mobtk.tryfourus.models.AppsCompanyModel
import com.mobtk.tryfourus.task.AppTask
import kotlinx.android.synthetic.main.fragment_detalle_administrador_uempresa_apps.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * A simple [Fragment] subclass.
 */
class DetalleAdministradorUEmpresaAppsFragment : Fragment() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var appsEmpresaAdmin: ArrayList<AppsCompanyModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.fragment_detalle_administrador_uempresa_apps,
            container,
            false
        )

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        appsEmpresaAdmin = ArrayList()
        AppsEmpresasAdmin()
    }

    private fun AppsEmpresasAdmin(){
        doAsync {
            val appProfile = AppTask.getApp(getString(R.string.base_url))
            uiThread {
                linearLayoutManager = LinearLayoutManager(context)
                rvAppsEmpresaAdmin.layoutManager = linearLayoutManager
                appsEmpresaAdmin = appProfile.aplica
                rvAppsEmpresaAdmin.adapter = AppsAdministradorAdapter(appsEmpresaAdmin)
            }
        }
    }

}
