package com.mobtk.tryfourus.fragments



import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import br.com.ilhasoft.support.validation.Validator
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.databinding.FragmentCompanyBinding
import kotlinx.android.synthetic.main.fragment_company.*

class CompanyFragment : Fragment(), Validator.ValidationListener {
    private lateinit var binding: FragmentCompanyBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_company, container, false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.clickListener = View.OnClickListener {
            when (it!!.id) {
                binding.btnRegistroEmpresa.id -> {
                    validator!!.toValidate()
                }
            }
        }
    }


    override fun onValidationError() {
    }

    override fun onValidationSuccess() {
    }
}

