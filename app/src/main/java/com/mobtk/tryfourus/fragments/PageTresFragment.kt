package com.example.myapplication


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.activities.CertificadoActivity
import kotlinx.android.synthetic.main.fragment_page_tres.*

class PageTresFragment : Fragment() {
        override fun onCreateView(
                inflater: LayoutInflater, container: ViewGroup?,
                savedInstanceState: Bundle?
        ): View? {
                // Inflate the layout for this fragment
                return inflater.inflate(R.layout.fragment_page_tres, container, false)
        }


        override fun onActivityCreated(state: Bundle?) {
                super.onActivityCreated(state)
        }
}
