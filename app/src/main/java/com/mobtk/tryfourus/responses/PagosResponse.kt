package com.mobtk.tryfourus.responses

import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.PagosModel

data class PagosResponse(@SerializedName("pagos") var pagos: ArrayList<PagosModel>,
                         @SerializedName("valido") var valid: String?=null
                         )