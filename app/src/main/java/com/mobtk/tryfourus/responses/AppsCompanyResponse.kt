package com.mobtk.tryfourus.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.AppsCompanyModel

class AppsCompanyResponse (@SerializedName("aplicaciones") @Expose val aplica:ArrayList<AppsCompanyModel>)