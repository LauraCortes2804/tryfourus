package com.mobtk.tryfourus.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.NotificacionModel

class NotificacionesResponse(@SerializedName("valido") var valido:String,
@SerializedName("notificaciones") var notificacione:ArrayList<NotificacionModel>)
