package com.mobtk.tryfourus.responses

import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.UsuariosEmpresaModel

class UsuariosEmpresasResponse (@SerializedName("usuariosEmpresa") var usuariosEmpresa: ArrayList<UsuariosEmpresaModel>) {
}