package com.mobtk.tryfourus.responses

import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.CategoriasAppsModel

data class CategoriasAppsResponse (@SerializedName ("categorias") var categoriasApps:ArrayList<CategoriasAppsModel>)