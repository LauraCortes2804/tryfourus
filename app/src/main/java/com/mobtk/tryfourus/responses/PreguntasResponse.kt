package com.mobtk.tryfourus.responses

import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.PreguntaModel

data class PreguntasResponse(@SerializedName("valido") var valid: String?=null,
                             @SerializedName("preguntas") var preguntas: ArrayList<PreguntaModel>)