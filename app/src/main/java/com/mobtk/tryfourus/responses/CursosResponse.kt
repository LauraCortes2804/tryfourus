package com.mobtk.tryfourus.responses

import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.CursosModel
import com.mobtk.tryfourus.models.PagosModel

data class CursosResponse(@SerializedName("cursos") var cursosL: ArrayList<CursosModel>

)