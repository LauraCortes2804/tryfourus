package com.mobtk.tryfourus.responses

import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.UsuariosTesterModel

class UsuariosTestersResponse (@SerializedName("usuariosTester") var usuariosTester:ArrayList<UsuariosTesterModel>){
}