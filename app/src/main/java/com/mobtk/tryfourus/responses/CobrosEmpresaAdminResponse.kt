package com.mobtk.tryfourus.responses

import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.CobrosEmpresaAdminModel

class CobrosEmpresaAdminResponse (@SerializedName("cobros")val cobros:ArrayList<CobrosEmpresaAdminModel>)