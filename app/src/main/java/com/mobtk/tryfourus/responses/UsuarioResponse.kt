package com.mobtk.tryfourus.responses

import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.UsuarioModel

data class UsuarioResponse(@SerializedName("valido") var valido:String,
                        @SerializedName("usuario") var user: UsuarioModel,
                        @SerializedName("mensaje") var mensaje: String)