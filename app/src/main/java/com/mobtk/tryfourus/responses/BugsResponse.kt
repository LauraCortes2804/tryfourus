package com.mobtk.tryfourus.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.models.BugsTesterModel

data class BugsResponse (@SerializedName ("repo_bubg") @Expose val reporteBug: ArrayList<BugsTesterModel>)