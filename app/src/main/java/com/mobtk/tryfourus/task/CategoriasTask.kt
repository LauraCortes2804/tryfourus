package com.mobtk.tryfourus.task

import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.responses.CategoriasAppsResponse
import com.mobtk.tryfourus.utilities.RetrofitConstructor

class CategoriasTask {

    companion object {
        fun getCategoriasApps(url: String): CategoriasAppsResponse {
            val call = RetrofitConstructor.getRetrofit(url).
                create(ApiService::class.java).categoriasApps().execute()
            val categorias = call.body() as CategoriasAppsResponse
            return categorias
        }
    }
}