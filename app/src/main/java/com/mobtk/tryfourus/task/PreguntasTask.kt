package com.mobtk.tryfourus.task

import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.responses.PreguntasResponse
import com.mobtk.tryfourus.utilities.RetrofitConstructor

class PreguntasTask {
    companion object {
        fun getPreguntas(url: String): PreguntasResponse {
            val call =
                RetrofitConstructor.getRetrofit(url).create(ApiService::class.java).getPreguntas()
                    .execute()
            val user = call.body() as PreguntasResponse
            return user
        }
    }
}