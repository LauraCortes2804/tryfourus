package com.mobtk.tryfourus.task

import com.mobtk.tryfourus.R
import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.responses.BugsResponse
import com.mobtk.tryfourus.utilities.RetrofitConstructor

class BugsTask {

    companion object{
        fun getBugs(url_base: String): BugsResponse{
            val call = RetrofitConstructor.getRetrofit(url_base).create(
                ApiService::class.java).bugsTester().execute()
            val response = call.body() as BugsResponse
            return response

        }
    }
}