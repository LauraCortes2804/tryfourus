package com.mobtk.tryfourus.task

import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.responses.UsuariosEmpresasResponse
import com.mobtk.tryfourus.responses.UsuariosTestersResponse
import com.mobtk.tryfourus.utilities.RetrofitConstructor

class TesterTask {
    companion object{
        fun getTesters(url:String):UsuariosTestersResponse{
            val call = RetrofitConstructor.getRetrofit(url).create(
                ApiService::class.java)
                .getTester().execute()
            val response =call.body() as UsuariosTestersResponse

            return response
        }
    }
}