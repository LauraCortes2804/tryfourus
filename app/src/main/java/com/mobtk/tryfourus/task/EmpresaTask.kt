package com.mobtk.tryfourus.task

import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.responses.UsuariosEmpresasResponse
import com.mobtk.tryfourus.utilities.RetrofitConstructor

class EmpresaTask {
    companion object{
        fun getEmpresas(url:String):UsuariosEmpresasResponse{
            val call = RetrofitConstructor.getRetrofit(url).create(
                ApiService::class.java)
                .getEmpresa().execute()
            val response =call.body() as UsuariosEmpresasResponse

            return response
        }
    }
}