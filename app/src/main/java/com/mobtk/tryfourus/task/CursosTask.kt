package com.mobtk.tryfourus.task

import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.responses.CursosResponse
import com.mobtk.tryfourus.utilities.RetrofitConstructor

class CursosTask {
    companion object {
        fun getCursos(url: String): CursosResponse {
            val call = RetrofitConstructor.getRetrofit(url).
                create(ApiService::class.java).cursos().execute()
            val cursos = call.body() as CursosResponse
            return cursos
        }
    }
}