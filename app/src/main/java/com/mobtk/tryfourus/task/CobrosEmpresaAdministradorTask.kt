package com.mobtk.tryfourus.task

import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.responses.CobrosEmpresaAdminResponse
import com.mobtk.tryfourus.utilities.RetrofitConstructor

class CobrosEmpresaAdministradorTask {
    companion object {
        fun getCobrosEmpresaAdmin(url: String): CobrosEmpresaAdminResponse {
            val call =
                RetrofitConstructor.getRetrofit(url).create(ApiService::class.java).getCobrosEmpresaAdmin()
                    .execute()
            val result = call.body() as CobrosEmpresaAdminResponse
            return result
        }
    }
}