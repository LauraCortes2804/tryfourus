package com.mobtk.tryfourus.task

import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.responses.PagosResponse
import com.mobtk.tryfourus.utilities.RetrofitConstructor

class UserTask {
    companion object {
        fun getPagos(url: String): PagosResponse {
            val call =
                RetrofitConstructor.getRetrofit(url).create(ApiService::class.java).getPagos()
                    .execute()
            val user = call.body() as PagosResponse
            return user
        }
    }
}