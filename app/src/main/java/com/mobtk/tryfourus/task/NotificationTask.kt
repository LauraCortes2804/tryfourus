package com.mobtk.tryfourus.task

import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.responses.NotificacionesResponse
import com.mobtk.tryfourus.responses.PagosResponse
import com.mobtk.tryfourus.utilities.RetrofitConstructor

class NotificationTask {
    companion object {
        fun getNotificaciones(url: String): NotificacionesResponse {
            val call =
                RetrofitConstructor.getRetrofit(url).create(ApiService::class.java).getNotificaciones()
                    .execute()
            val user = call.body() as NotificacionesResponse
            return user
        }
    }
}