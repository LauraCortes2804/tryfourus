package com.mobtk.tryfourus.task

import com.mobtk.tryfourus.responses.AppsCompanyResponse
import com.mobtk.tryfourus.interfaz.ApiService
import com.mobtk.tryfourus.utilities.RetrofitConstructor

class AppTask{
    companion object {
        fun getApp(url: String): AppsCompanyResponse {
            val call = RetrofitConstructor.getRetrofit(url).
                create(ApiService::class.java).getApps().execute()
            val apps = call.body() as AppsCompanyResponse
            return apps
        }
    }
}