package com.mobtk.tryfourus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobtk.tryfourus.utilities.DynamicSearchAdapter

class UsuariosTesterModel: DynamicSearchAdapter.Searchable {
                    @SerializedName("nombre") @Expose val nombreT: String=""
                    @SerializedName("apellidos") @Expose val apellidosT: String?=null
                    @SerializedName("correo") @Expose val correoT: String?=null
                    @SerializedName("fechaRegistro") @Expose val fechaRegistro: String?=null
    /** This method will allow to specify a search string to compare against
    your search this can be anything depending on your use case.
     */
    override fun getSearchCriteria(): String {
        return nombreT.toLowerCase()
    }
}