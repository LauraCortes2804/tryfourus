package com.mobtk.tryfourus.models

import com.google.gson.annotations.SerializedName

class CobrosEmpresaAdminModel (@SerializedName("nombre")val nombre:String,
                               @SerializedName("correo")val correo:String,
                               @SerializedName("cantidad")var cantidad:String,
                               @SerializedName("fechaPago")val fechaPago:String,
                               @SerializedName("tipo")val tipo:Int)

