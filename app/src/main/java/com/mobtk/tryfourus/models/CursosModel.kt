package com.mobtk.tryfourus.models

import com.google.gson.annotations.SerializedName

class CursosModel (@SerializedName("titulo")val tituloC:String,
                   @SerializedName("nivel")val nivelC:String,
                   @SerializedName("duracion")var duracionC:String,
                   @SerializedName("fechaRegistro")val fechaR:String)