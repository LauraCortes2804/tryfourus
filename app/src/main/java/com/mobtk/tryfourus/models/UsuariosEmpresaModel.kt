package com.mobtk.tryfourus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UsuariosEmpresaModel {
    @SerializedName("nombre") @Expose val nombreE : String? = null
    @SerializedName("correo") @Expose val correoE : String? = null
    @SerializedName("telefono") @Expose val telefonoE : String? = null
    @SerializedName("pais") @Expose val paisE : String? = null
    @SerializedName("fechaRegistro") @Expose val fechaRegistroE : String? = null

}