package com.mobtk.tryfourus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PreguntaModel{
    @SerializedName("pregunta_id") @Expose
    var id_pregunta: String?=null
    @SerializedName("pregunta") @Expose
    var pregunta: String?=null
    @SerializedName("pregunta_respuesta") @Expose
    var pregunta_respuesta: String?=null
}