package com.mobtk.tryfourus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NotificacionModel {
    @SerializedName("id_notificacion") @Expose
    var id_notificacion: String?=null
    @SerializedName("mensaje_notificacion") @Expose
    var mensaje: String?=null
    @SerializedName("fecha_notificacion") @Expose
    var fecha: String?=null
}