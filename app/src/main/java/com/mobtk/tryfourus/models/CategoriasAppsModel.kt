package com.mobtk.tryfourus.models

import com.google.gson.annotations.SerializedName

data class CategoriasAppsModel (@SerializedName ("nombre") var nombreCategoriaApp: String)