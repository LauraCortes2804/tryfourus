package com.mobtk.tryfourus.models

import com.google.gson.annotations.SerializedName

data class BugsTesterModel (@SerializedName ("bug_nombre") var nombreBug: String,
                            @SerializedName ("bug_reportados") val bugReportados: String,
                            @SerializedName ("bug_aprobados") val bugAprobados:String,
                            @SerializedName ("bug_desc") val bugDesc: String,
                            @SerializedName ("bug_cat") val bugCatego: String)