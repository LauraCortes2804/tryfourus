package com.mobtk.tryfourus.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PagosModel:Serializable  {
    @SerializedName("pago_id") @Expose
    var id_pago: String?=null
    @SerializedName("pago_nombre") @Expose
    var pago_nombre: String?=null
    @SerializedName("pago_usr") @Expose
    var pago_user: String?=null
    @SerializedName("pago_cantidad") @Expose
    var pago_cantidad: String?=null
    @SerializedName("pago_tarjeta") @Expose
    var pago_tarjeta: String?=null
    @SerializedName("pago_fecha") @Expose
    var pago_fecha: String?=null

}