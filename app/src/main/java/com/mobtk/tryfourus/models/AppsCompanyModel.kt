package com.mobtk.tryfourus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AppsCompanyModel (@SerializedName("nombre") @Expose val name: String,
                        @SerializedName("categoria") @Expose val categoria: String,
                        @SerializedName("version") @Expose val version: String,
                        @SerializedName("limiteTester") @Expose val limites:String,
                        @SerializedName("img") @Expose val imagen:String):Serializable