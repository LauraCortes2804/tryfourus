package com.mobtk.tryfourus.utilities

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPageAdapter (manager : FragmentManager): FragmentPagerAdapter(manager){

    private  val fragmentListA : MutableList<Fragment> = ArrayList()
    private  val titleListA : MutableList<String> = ArrayList()
    override fun getItem(position: Int): Fragment {
        return fragmentListA[position]
    }

    override fun getCount(): Int {
        return fragmentListA.size
    }

    fun addFragment(fragment: Fragment, title:String){
        fragmentListA.add(fragment)
        titleListA.add(title)

    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titleListA[position]
    }

}